package com.alibaba.gao.delayQueue.processor;

/**
 * <p>
 * Title:接口类
 * </p>
 * <p>
 * Description:redis延迟消息处理接口类
 * </p>
 * <p>
 * Copyright: 2020~2021
 * </p>
 * <p>
 * Company/Department: EnnCloud
 * </p>
 *
 * @author zhangyiwu <b>Creation Time:</b> 2021-11-22 15:10:28
 * @since V1.0
 */
public interface RedisDelayQueueHandler<T> {

    /**
     * redis延时队列消息处理
     *
     * @param t
     *            消息
     */
    void execute(T t);
}
