package com.alibaba.gao.delayQueue.processor.handler;

import java.util.Map;

import com.alibaba.gao.delayQueue.processor.RedisDelayQueueHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * Title:处理器
 * </p>
 * <p>
 * Description:工单未接单延时消息处理器
 * </p>
 * <p>
 * Copyright: 2020~2021
 * </p>
 * <p>
 * Company/Department: EnnCloud
 * </p>
 *
 * @author zhangyiwu <b>Creation Time:</b> 2021-11-22 15:17:04
 * @since V1.0
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OrderNotAcceptedHandler implements RedisDelayQueueHandler<Map<String, String>> {

    /**
     * 消息通知服务
     */
    //private final NotifyBizService notifyBizService;

    /**
     * 工单服务
     */
    //private final WorkOrderService workOrderService;

    @Override
    public void execute(Map<String, String> map) {
        log.info("从redis中获取到的未接单工单消息为：{}", map);
        if (CollectionUtils.isEmpty(map)) {
            log.info("redis消息内容为空，无需处理。");
            return;
        }

        // 获取工单信息
//        WorkOrderPO workOrder =
//            this.getWorkOrder(map.get(WorkOrderConstants.TENANT_ID), map.get(WorkOrderConstants.WORK_ORDER_NO));
        // 工单信息为空，不做处理
//        if (Objects.isNull(workOrder)) {
//            log.info("工单信息未获取到，无需处理。");
//            return;
//        }

        // 工单状态不为待接单，不做处理
//        if (!WorkOrderStatusEnum.STATUS_WAIT_FOR_ACCEPT.getCode().equals(workOrder.getWorkOrderStatus())) {
//            log.info("工单状态已不为待接单状态，无需处理。");
//            return;
//        }

        // 重新发送接单通知
//        this.reNotify(workOrder);
    }

    /**
     * 根据租户id、通知类型和工单编号获取通知历史记录
     *
     * @param tenantId
     *            租户id
     * @param workOrderNo
     *            工单编号
     * @return WorkOrderNotifyHistoryPO
     */
//    private WorkOrderPO getWorkOrder(String tenantId, String workOrderNo) {
//        return workOrderService.getOne(new LambdaQueryWrapper<WorkOrderPO>().eq(WorkOrderPO::getTenantId, tenantId)
//            .eq(WorkOrderPO::getWorkOrderNo, workOrderNo));
//    }

    /**
     * 重新发送通知
     *
     * @param workOrder
     *            工单信息
     */
//    private void reNotify(WorkOrderPO workOrder) {
//        NotifyDTO notifyDto = NotifyDTO.builder().tenantId(workOrder.getTenantId())
//            .receiverId(workOrder.getExecutorId()).senderName(workOrder.getDispatcherName())
//            .workOrderNo(workOrder.getWorkOrderNo()).workOrderName(workOrder.getWorkOrderName())
//            .operateTime(workOrder.getDispatchTime()).statusEnum(WorkOrderStatusEnum.STATUS_WAIT_FOR_ACCEPT).build();
//        notifyBizService.sendNotify(NotifyTypeEnum.WORK_ORDER_DISPATCHED, notifyDto);
//    }
}
