package com.alibaba.gao.controller;

import com.alibaba.gao.cache.CommonCacheService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("user")
public class UserController {

    @Resource
    CommonCacheService commonCacheService;

    @GetMapping("test1")
    public void test(){
        commonCacheService.cacheData("gaohaicheng","name","高海成");
        Object cacheData = commonCacheService.getCacheData("gaohaicheng", "name");
        System.out.println(cacheData);
    }

}

