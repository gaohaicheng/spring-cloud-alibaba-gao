package com.alibaba.gao.cache;

/**
 * Title:
 * Description:
 * Copyright:2022~2099
 * Company/Department:xin ao xin zhi
 *
 * @author Lichengyi
 * Creation Time: 2022/5/17 14:33
 * @since V1.0
 */
public interface CommonCacheService {
    /**
     * 缓存值
     *
     * @param prefix 前缀
     * @param key    key值
     * @param value  值
     * @author pengpengc
     * @since 2022/6/6 14:53
     **/
    <T> void cacheData(String prefix, String key, T value);

    /**
     * @param prefix  前缀
     * @param key     key值
     * @param value   值
     * @param timeout 超时时间
     * @author pengpengc
     * @since 2022/6/6 14:55
     **/
    <T> void cacheData(String prefix, String key, T value, long timeout);

    /**
     * 获取缓存值
     *
     * @param prefix 前缀
     * @param key    key值
     * @return T 值
     * @author pengpengc
     * @since 2022/6/6 14:55
     **/
    <T> T getCacheData(String prefix, String key);

    /**
     * 判断是否存在
     *
     * @param prefix 前缀
     * @param key    key值
     * @return java.lang.Boolean
     * @author pengpengc
     * @since 2022/6/6 14:56
     **/
    Boolean checkKeyExist(String prefix, String key);

    /**
     * 删除key
     *
     * @param prefix 前缀
     * @param key    key值
     * @return java.lang.Boolean
     * @author pengpengc
     * @since 2022/6/6 14:57
     **/
    Boolean removeCacheKey(String prefix, String key);

}
