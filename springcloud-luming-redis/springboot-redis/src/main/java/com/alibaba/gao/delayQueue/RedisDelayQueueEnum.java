 package com.alibaba.gao.delayQueue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * <p>
 * Title:枚举类
 * </p>
 * <p>
 * Description:redis延迟队列枚举类
 * </p>
 * <p>
 * Copyright: 2020~2021
 * </p>
 * <p>
 * Company/Department: EnnCloud
 * </p>
 *
 * @author zhangyiwu <b>Creation Time:</b> 2021-11-22 15:04:40
 * @since V1.0
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum RedisDelayQueueEnum {

    ORDER_NOT_ACCEPTED("ORDER_NOT_ACCEPTED", "工单长时间处于未接单状态", "orderNotAcceptedHandler");

    /**
     * 延迟队列 Redis Key
     */
    private String code;

    /**
     * 中文描述
     */
    private String name;

    /**
     * 延迟队列具体业务实现的 Bean,可通过 Spring 的上下文获取
     */
    private String beanId;
}
