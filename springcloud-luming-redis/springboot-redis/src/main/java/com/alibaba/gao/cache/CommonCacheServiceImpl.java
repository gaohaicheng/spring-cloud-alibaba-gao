package com.alibaba.gao.cache;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Title:
 * Description:
 * Copyright:2022~2099
 * Company/Department:xin ao xin zhi
 *
 * @author Lichengyi
 * Creation Time: 2022/5/17 14:34
 * @since V1.0
 */

@Slf4j
@Service
public class CommonCacheServiceImpl implements CommonCacheService {

    /**
     * appid
     */
    @Value("${app.id}")
    private String appId;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    @Override
    public <T> void cacheData(String prefix, String key, T value) {
        log.trace("That app setting the cache info includes key is {},value is {}.", key, null != value ? value.toString() : StringUtils.EMPTY);
        String prefixKey = appId+":"+prefix+":" + key;
        if(ObjectUtil.isNull(value)){
            return;
        }
        this.redisTemplate.opsForValue().set(prefixKey,value);
    }

    @Override
    public <T> void cacheData(String prefix, String key, T value, long timeout) {
        log.trace("That app setting the cache info includes key is {},value is {} timeout is {}.", key, null != value ? value.toString() : StringUtils.EMPTY, timeout);
        String prefixKey = appId+":"+prefix+":" + key;
        this.redisTemplate.opsForValue().set(prefixKey, value, timeout, TimeUnit.MILLISECONDS);
    }


    @Override
    public <T> T getCacheData(String prefix, String key) {
        String prefixKey = appId+":"+prefix+":" + key;
        T value = (T) this.redisTemplate.opsForValue().get(prefixKey);
        log.trace("The getting cache info includes key is {},  which result is {}. ", key, null != value ? value.toString() : StringUtils.EMPTY);
        return value;
    }

    @Override
    public Boolean checkKeyExist(String prefix, String key) {
        String prefixKey = appId+":"+prefix+":" + key;
        Object value = this.redisTemplate.opsForValue().get(prefixKey);
        log.trace("The check key exist {},  which result is {}. ", key, value);
        return value != null;
    }

    @Override
    public Boolean removeCacheKey(String prefix, String key) {
        Boolean delete = this.redisTemplate.delete(appId+":"+prefix+":" + key);
        log.trace("The remove cache key {},  which result is {}. ", key,delete);
        return delete;
    }
}
