package com.alibaba.gao.filter;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.gao.util.JWTUtil;

import io.jsonwebtoken.Claims;
import reactor.core.publisher.Mono;

/**
 * Spring Cloud Gateway整合JWT现统一认证和鉴权   https://blog.csdn.net/qq_41925205/article/details/114284614
 * 将登录用户的JWT转化成用户信息的全局过滤器
 * 
 * 网关服务
 */
@Component
public class AuthGlobalFilter implements GlobalFilter {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthGlobalFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getHeaders().getFirst("token");
        ServerHttpResponse response = exchange.getResponse();
        if (StringUtils.isEmpty(token)) {
            JSONObject message = new JSONObject();
            message.put("code", HttpStatus.UNAUTHORIZED);
            message.put("message", "鉴权失败，无token或类型");
            byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
            DataBuffer buffer = response.bufferFactory().wrap(bits);
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.getHeaders().add("Content-Type", "text/json;charset=UTF-8");
            return response.writeWith(Mono.just(buffer));
        }
        try {
            //从token中解析用户信息并设置到Header中去
            Claims claims = JWTUtil.parseJWT(token);
            String userStr = claims.getSubject();
            LOGGER.info("AuthGlobalFilter.filter() user:{}", userStr);
            ServerHttpRequest request = exchange.getRequest().mutate().header("user", userStr).build();
            exchange = exchange.mutate().request(request).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chain.filter(exchange);
    }
}

