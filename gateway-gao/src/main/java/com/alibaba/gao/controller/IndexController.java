package com.alibaba.gao.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@Api(tags = "SpringbootAdmin-server 首页模块")
@RestController  //http://192.168.1.191:80/${servlet:context-path}/sayHi?name="高"

@RefreshScope
public class IndexController {  

	@NacosValue(value = "${config.info:}", autoRefreshed = true)
    private String config;
	@Value("${config.info:}")
	private String test;
	@Value("${gao.hai.cheng:}")
	private String gaohaicheng;
	@Value("${dev.config.gao-dev:}")
	private String testConfig;
	@Value("${dev.config.test:}")
	private String devTest;
	
	private RouteDefinition router;
	
    @ApiImplicitParam(name = "name",value = "姓名",required = true)
    @ApiOperation(value = "向客人问好")
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name")String name){
        return ResponseEntity.ok("Hi:"+name + "gaohaicheng: " + gaohaicheng +  "   config : "+config + "   test : "+test + "   testConfig : "+testConfig + "  devTest: "+ devTest);
    }
}