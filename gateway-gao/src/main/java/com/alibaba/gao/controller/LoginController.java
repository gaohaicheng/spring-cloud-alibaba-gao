package com.alibaba.gao.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.gao.util.JWTUtil;
/**
 * Spring Cloud Gateway整合JWT现统一认证和鉴权   https://blog.csdn.net/qq_41925205/article/details/114284614
 * @ClassName: LoginController
 * @Description: TODO
 * @author Luming
 * @date 2021-06-29 01:11:30
 * 
 * 
 * 登录服务
 * 
 */
@RestController
@RequestMapping("user")
public class LoginController {

//    @PostMapping("/login")
//    public Map<String, Object> login(@RequestBody Map<String, String> user) {
//        Map<String, Object> result = new HashMap<String, Object>();
//        Boolean login = user.get("username").equals("test") && user.get("password").equals("1234");
//        if (login) {
//            Map<String, String> info = new HashMap<>();
//            info.put("username", user.get("username"));
//            String token = JWTUtil.createJWT(UUID.randomUUID().toString(), user.get("username"),
//                    null);
//            info.put("token", token);
//            result.put("200", "登录成功");
//            result.put("info", info);
//        } else {
//            result.put("401", "用户名或密码错误");
//        }
//        return result;
//    }
}

