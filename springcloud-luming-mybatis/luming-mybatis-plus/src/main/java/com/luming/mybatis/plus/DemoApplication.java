package com.luming.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.luming.resultexception.handler.annotation.EnableGlobalDispose;


@EnableGlobalDispose //开启自定义的异常处理 和结果封装
@SpringBootApplication
@MapperScan("com.luming.mybatis.plus.mapper")
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
