package com.luming.mybatis.plus.service.impl;

import com.luming.mybatis.plus.entity.User;
import com.luming.mybatis.plus.mapper.UserMapper;
import com.luming.mybatis.plus.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-06-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
