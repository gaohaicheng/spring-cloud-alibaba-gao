package com.luming.mybatis.plus.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc  //@EnableSwagger2WebMvc原先是用@EnableSwagger2来做注解的，最新版本直接使用@EnableSwagger2WebMvc来做注解，暂时可以把他理解为功能就是一样的
@EnableKnife4j //@EnableKnife4j API接口文档的开关
@Import(BeanValidatorPluginsConfiguration.class)
@ConditionalOnExpression("${knife4j.enable}") //开启访问接口文档的权限  **knife4j.enable是在yml配置文件中配置为true**
public class SwaggerConfiguration {

//如果要设置多个用户组，只需要在定义一个Docket并打上@Bean返回即可
   @Bean
   public Docket docket(Environment environment) {

        //设置要显示的在线接口文档环境
        Profiles profiles = Profiles.of("dev","test");
        //通过environment.acceptsProfiles判断是否处于当前自己设定的环境中
        boolean flag = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
            	//是否在浏览器显示,如果一直要显示开启，就选择true
                .enable(flag)
             	//.enable(true)
                //分组名称
                .groupName("1.0版本")
                .select()
                //这里指定Controller扫描包路径(项目路径也行)
                .apis(RequestHandlerSelectors.basePackage("com.csdn.demo1.controller"))
                .paths(PathSelectors.any())
                .build();
    }
    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口说明")
                .description("DEMO服务接口说明")
                .termsOfServiceUrl("http://localhost:8888/")
                .version("1.0")
                .build();
    }
}
