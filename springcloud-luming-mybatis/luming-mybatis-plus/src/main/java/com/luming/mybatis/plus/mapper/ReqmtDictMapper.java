package com.luming.mybatis.plus.mapper;

import com.luming.mybatis.plus.entity.ReqmtDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典管理表字段 Mapper 接口
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
public interface ReqmtDictMapper extends BaseMapper<ReqmtDict> {

}
