package com.luming.mybatis.plus.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luming.mybatis.plus.service.UserService;
import com.luming.resultexception.handler.result.Result;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-06-13
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@ExceptionHandler
	@RequestMapping("getUser/{id}")  //http://localhost:8081/user/getUser/1
    public Object GetUser(@PathVariable int id){
		return userService.getById(id);
		//return Result.ok().data("data", userService.getById(id)).message("查询用户成功");
    }
	
	
	
	

}
