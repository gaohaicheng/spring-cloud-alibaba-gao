package com.luming.mybatis.plus.service;

import com.luming.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-06-13
 */
public interface UserService extends IService<User> {

}
