package com.luming.mybatis.plus.service.impl;

import com.luming.mybatis.plus.entity.ReqmtDict;
import com.luming.mybatis.plus.mapper.ReqmtDictMapper;
import com.luming.mybatis.plus.service.ReqmtDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典管理表字段 服务实现类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@Service
public class ReqmtDictServiceImpl extends ServiceImpl<ReqmtDictMapper, ReqmtDict> implements ReqmtDictService {

}
