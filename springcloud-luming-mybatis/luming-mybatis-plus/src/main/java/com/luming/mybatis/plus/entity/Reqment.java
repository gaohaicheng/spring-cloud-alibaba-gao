package com.luming.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 需求表
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Reqment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 需求ID
     */
    private String reqmtId;

    /**
     * 需求名称
     */
    private String reqmtTitle;

    /**
     * 需求类型
     */
    private String reqmtType;

    /**
     * 需求版本
     */
    private String reqmtVersion;

    /**
     * 版本描述
     */
    private String versionDesc;

    /**
     * 需求优先级1-非常紧急，2-紧急，3-普通，4-较低
     */
    private String reqmtLevel;

    /**
     * 标签(字典表定义)
     */
    private String reqmtTags;

    /**
     * 需求背景
     */
    private String reqmtBground;

    /**
     * 目标价值
     */
    private String reqmtTagvalue;

    /**
     * 功能场景
     */
    private String reqmtFunction;

    /**
     * 需求提出时间
     */
    private LocalDateTime reqmtTime;

    /**
     * 需求状态(字典表定义)0-待发布，1-已发布，2-设计中，3-已完成，4-已驳回，5-已删除,6-开发中，7-已发布,8-草稿
     */
    private String reqmtStatus;

    /**
     * 需求人员ID
     */
    private String reqmtUserId;

    /**
     * 需求人员名称
     */
    private String reqmtUser;

    /**
     * 设计人员ID
     */
    private String designerId;

    /**
     * 设计人员名称
     */
    private String designerName;

    /**
     * 创建人ID
     */
    private String createUserId;

    /**
     * 创建人名称
     */
    private String createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人ID
     */
    private String updateUserId;

    /**
     * 更新人名称
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 删除状态0-未删除，1-已删除
     */
    private String delStatus;


}
