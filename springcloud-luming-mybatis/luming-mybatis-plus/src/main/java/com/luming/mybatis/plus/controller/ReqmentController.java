package com.luming.mybatis.plus.controller;


import org.springframework.web.bind.annotation.RestController;
import com.luming.mybatis.plus.entity.Reqment;
import com.luming.mybatis.plus.service.ReqmentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;

/**
 * <p>
 * 需求表 前端控制器
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@RestController
@RequestMapping("/reqment")
public class ReqmentController {

    @Resource
    private ReqmentService service;

    @PostMapping("/add")
    public boolean save(@RequestBody Reqment entity) {
        return service.save(entity);
    }

}
