package com.luming.mybatis.plus.mapper;

import com.luming.mybatis.plus.entity.Reqment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 需求表 Mapper 接口
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
public interface ReqmentMapper extends BaseMapper<Reqment> {

}
