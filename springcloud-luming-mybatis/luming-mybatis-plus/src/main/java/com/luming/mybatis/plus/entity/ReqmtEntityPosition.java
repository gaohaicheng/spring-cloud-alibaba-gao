package com.luming.mybatis.plus.entity;

import lombok.Data;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName ReqmtEntityPosition.java
 * @Description TODO
 * @createTime 2021年10月13日 13:36:00
 */
@Data
public class ReqmtEntityPosition {

    // 词
    private String word;

    private String type;

    private Integer status;

    private Integer start;

    private Integer end;


}
