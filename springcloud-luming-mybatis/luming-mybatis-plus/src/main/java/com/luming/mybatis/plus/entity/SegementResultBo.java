package com.luming.mybatis.plus.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class SegementResultBo implements Serializable {

    private List<ReqmtEntityPosition> reqmtCaseList = new ArrayList<>();
    private List<ReqmtEntityPosition> reqmtUserList = new ArrayList<>();
    private List<ReqmtEntityPosition> reqmtActionList = new ArrayList<>();
    private List<ReqmtEntityPosition> reqmtRuleList = new ArrayList<>();
    private List<ReqmtEntityPosition> reqmtDomainList = new ArrayList<>();


    public void addSegment(ReqmtEntityPosition reqmtEntityPosition){
       if(CustomWordSegmentEnum.REQMTCASE.getCode().equals(reqmtEntityPosition.getType())){
           reqmtCaseList.add(reqmtEntityPosition);
       }else if (CustomWordSegmentEnum.REQMTUSER.getCode().equals(reqmtEntityPosition.getType())){
               reqmtUserList.add(reqmtEntityPosition);
       }else if (CustomWordSegmentEnum.REQMTACTION.getCode().equals(reqmtEntityPosition.getType())){
               reqmtActionList.add(reqmtEntityPosition);
       }else if (CustomWordSegmentEnum.REQMTRULE.getCode().equals(reqmtEntityPosition.getType())){
               reqmtRuleList.add(reqmtEntityPosition);
       }else if (CustomWordSegmentEnum.REQMTDOMAIN.getCode().equals(reqmtEntityPosition.getType())){
               reqmtDomainList.add(reqmtEntityPosition);
       }
    }



}
