package com.luming.mybatis.plus.mapper;

import com.luming.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-06-13
 */
public interface UserMapper extends BaseMapper<User> {

}
