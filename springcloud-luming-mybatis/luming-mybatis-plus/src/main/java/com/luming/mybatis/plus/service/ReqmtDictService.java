package com.luming.mybatis.plus.service;

import com.luming.mybatis.plus.entity.ReqmtDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典管理表字段 服务类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
public interface ReqmtDictService extends IService<ReqmtDict> {

}
