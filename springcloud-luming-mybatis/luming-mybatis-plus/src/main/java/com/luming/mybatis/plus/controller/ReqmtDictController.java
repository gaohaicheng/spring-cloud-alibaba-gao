package com.luming.mybatis.plus.controller;


import cn.hutool.core.util.StrUtil;
import com.luming.mybatis.plus.entity.CustomWordSegmentEnum;
import com.luming.mybatis.plus.entity.Reqment;
import com.luming.mybatis.plus.entity.ReqmtEntityPosition;
import com.luming.mybatis.plus.entity.SegementResultBo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * 字典管理表字段 前端控制器
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@RestController
@RequestMapping("/reqmt-dict")
public class ReqmtDictController {

    String html = "";


    @PostMapping("/add")
    public String save(@RequestBody Reqment entity) {
        String documentTxt=entity.getVersionDesc();
        Pattern p = Pattern.compile("\n");
        Matcher matcher = p.matcher(documentTxt);
        boolean b =matcher.find();
        if(b){
            System.out.println("存在回车         " );
        }else{
            System.out.println("不存在回车");
        }
        //System.out.println(documentTxt);

        documentTxt = documentTxt.replaceAll("</p>|</h[1-9]?>", "\n");
        documentTxt = documentTxt.replaceAll("&nbsp;", " ");
        documentTxt = documentTxt.replaceAll("<(\\S*?)[^>]*>.*?|<.*? /> ", "");



       /* String[] split = documentTxt.split("\n");
        for (String s : split) {
            System.out.println("****** "+s);
        }
*/
        System.out.println(documentTxt);

        this.html = documentTxt;


        return documentTxt;
    }






    @PostMapping("/query")
    public SegementResultBo query(@RequestBody Reqment entity) {
        SegementResultBo bo = new SegementResultBo();

        // TODO 分词集合，然后去重放入下面集合中

        List<String> reqmtCaseList = new ArrayList<>();
        reqmtCaseList.add("aa");
        reqmtCaseList.add("aa");
        reqmtCaseList.add("aa");
        List<String> reqmtUserList = new ArrayList<>();
        reqmtUserList.add("");
        reqmtUserList.add("");
        reqmtUserList.add("");
        List<String> reqmtActionList = new ArrayList<>();
        reqmtActionList.add("aaa");
        reqmtActionList.add("aaa");
        reqmtActionList.add("aaa");
        List<String> reqmtRuleList = new ArrayList<>();
        reqmtRuleList.add("aa");
        reqmtRuleList.add("aa");
        reqmtRuleList.add("aa");
        List<String> reqmtDomainList = new ArrayList<>();
        reqmtDomainList.add("aaa");
        reqmtDomainList.add("aaa");
        reqmtDomainList.add("aaa");


        String content = html;
        //记录位置集合，防止位置重复；
        List<String> positions = new ArrayList<>();


        extracted(bo, reqmtCaseList, content, positions,CustomWordSegmentEnum.REQMTCASE);
        extracted(bo, reqmtUserList, content, positions,CustomWordSegmentEnum.REQMTUSER);
        extracted(bo, reqmtActionList, content, positions,CustomWordSegmentEnum.REQMTACTION);
        extracted(bo, reqmtRuleList, content, positions,CustomWordSegmentEnum.REQMTRULE);
        extracted(bo, reqmtDomainList, content, positions,CustomWordSegmentEnum.REQMTDOMAIN);
        return bo;
    }

    private void extracted(SegementResultBo bo, List<String> reqmtList, String content, List<String> positions,CustomWordSegmentEnum cusEnum) {
        for (String sToFind : reqmtList) {
            int extracted = this.extracted(content, positions, sToFind, 1);
            if(extracted != -1){
                ReqmtEntityPosition position = new ReqmtEntityPosition();
                position.setWord(sToFind);
                position.setType(cusEnum.getCode());
                position.setStatus(0);
                position.setStart(extracted);
                position.setEnd(extracted + sToFind.length());
                bo.addSegment(position);
            }
        }
    }

    private int extracted(String content, List<String> positions, String sToFind ,int oridinal) {
        if(content.contains(sToFind)) {
            int indexOf = StrUtil.ordinalIndexOf(content, sToFind, oridinal);

            if(indexOf == -1){
                return -1;
            }

            if (!positions.contains(String.valueOf(indexOf))) {
                positions.add(String.valueOf(indexOf));
                return indexOf;
            }else{
                oridinal = oridinal + 1;
                return this.extracted(content,positions,sToFind,oridinal);
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        List<String> positions = new ArrayList<>();
        String content = "身份两个了第三方机构\n" +
                "老师打分结果拉萨大家发过来反垄断机构拉萨大家发给老师的飓风格雷斯的发送了估计是的浪费" +
                "国家卡霍夫卡是分开时刻提防哈客户发卡号反馈阿斯顿霍夫卡霍夫卡霍夫卡霍夫卡货到付款 客户管理主要包括：客户工商信息自动补全，" +
                "工商数据完整呈现。客户对方的防守但是立法机关拉萨大家发给的结果士大夫概率事件的概率是大家跟进、成交、交付、服务、回款等信息、" +
                "全方位归集。客户智能化分级分类，落地差异化服务。客户公海，自动化分配回收，保有量规则，促进客户转化。客户主数据管理，保障客户数据" +
                "质量等功能 商机管理主要包括：销售最佳实践数字化老师落地，定义销售标准动作，规范销售过程。全方位构建干系人关系视图，快速定位关键决策人。销售漏斗、" +
                "预测，及时精准决策。商机作战地图，支持高效科学打单。商机结构化报价，提升成单效率等功能 活动管理主要包括了时长营销策划，预算审批，活动执行，" +
                "市场R但是发射点房间阿里山的房间阿里山的房间我去饿金融七五九二佛为甲方放假啦上的飞机啊拉萨大家发了是点击发送OI分析等。输入 weditor_5.vue?b544:419 onblur\n";


        String sToFind = "老师";


        ReqmtDictController reqmt = new ReqmtDictController();
        int extracted = reqmt.extracted(content, positions, sToFind, 1);

        System.out.println(extracted);


    }



}
