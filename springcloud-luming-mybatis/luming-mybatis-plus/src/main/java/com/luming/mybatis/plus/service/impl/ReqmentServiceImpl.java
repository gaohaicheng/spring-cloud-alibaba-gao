package com.luming.mybatis.plus.service.impl;

import com.luming.mybatis.plus.entity.Reqment;
import com.luming.mybatis.plus.mapper.ReqmentMapper;
import com.luming.mybatis.plus.service.ReqmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 需求表 服务实现类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@Service
public class ReqmentServiceImpl extends ServiceImpl<ReqmentMapper, Reqment> implements ReqmentService {

}
