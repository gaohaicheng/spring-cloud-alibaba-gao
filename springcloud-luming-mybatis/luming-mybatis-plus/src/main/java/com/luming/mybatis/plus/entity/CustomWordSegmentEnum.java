package com.luming.mybatis.plus.entity;


import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * 自定义分词类型
 */
public enum CustomWordSegmentEnum {

    REQMTCASE("case", "系统用例"),
    REQMTUSER("user", "用例参与者"),
    REQMTACTION("action", "业务活动"),
    REQMTRULE("rule", "业务规则"),
    REQMTDOMAIN("domain", "实体对象"),
    REQMTATTRIBUTE("attribute", "对象属性");

    private String code;

    private String msg;


    CustomWordSegmentEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static String getNameByCode(Integer code) {
        if (Objects.isNull(code)) {
            return StringUtils.EMPTY;
        }
        String name = StringUtils.EMPTY;
        for (CustomWordSegmentEnum value : CustomWordSegmentEnum.values()) {
            if (code.equals(value.code)) {
                name = value.msg;
                break;
            }
        }
        return name;
    }

}
