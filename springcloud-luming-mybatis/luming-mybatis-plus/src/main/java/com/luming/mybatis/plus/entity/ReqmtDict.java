package com.luming.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字典管理表字段
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ReqmtDict implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 词典id
     */
    private String dictId;

    /**
     * 名称
     */
    private String name;

    /**
     * 内容
     */
    private String dictContent;

    /**
     * 状态 0 未启用，1 启用
     */
    private Boolean dictStatus;

    /**
     * 词典使用频次
     */
    private Integer dictFrequency;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人id
     */
    private Long creatorId;

    /**
     * 创建人
     */
    private String creatorName;

    /**
     * 修改人
     */
    private Long modifier;

    /**
     * 创建人
     */
    private String modifierName;

    /**
     * 逻辑删除标识(0.未删除,1.已删除)
     */
    private Boolean isDel;


}
