package com.luming.mybatis.plus.service;

import com.luming.mybatis.plus.entity.Reqment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 需求表 服务类
 * </p>
 *
 * @author luming 鹿鸣
 * @since 2021-09-27
 */
public interface ReqmentService extends IService<Reqment> {

}
