package com.example.springboot.mapper.DB3Dao;

import com.example.springboot.pojo.Goods;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface GoodsDao {

    List<Goods> getAllGoods();
}
