package com.example.springboot.service.impl;

import com.example.springboot.common.DBTypeEnum;
import com.example.springboot.config.DataSourceSwitch;
import com.example.springboot.mapper.DB2Dao.BookDao;
import com.example.springboot.pojo.Book;
import com.example.springboot.service.BookService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.sql.DataSourceDefinition;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao bookDao;

    @DataSourceSwitch(DBTypeEnum.db2)
    @Override
    public List<Book> getAllBook() {
        return bookDao.selectAllBook();
    }
}
