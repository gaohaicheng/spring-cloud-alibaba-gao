package com.example.springboot.mapper.DB1Dao;


import com.example.springboot.pojo.User;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserDao{

    List<User> selectAllUser();
}
