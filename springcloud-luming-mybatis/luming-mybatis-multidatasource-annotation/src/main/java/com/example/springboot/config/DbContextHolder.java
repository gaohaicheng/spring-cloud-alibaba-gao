package com.example.springboot.config;

import com.example.springboot.common.DBTypeEnum;

/**
 * @author ：linyf
 * @date ：Created in 2019/4/11 17:18
 * @description：设置，获取，清空 当前线程内的数据源变量。
 */
public class DbContextHolder {

    private static final ThreadLocal contextHolder = new ThreadLocal<>();
    /**
     * 设置数据源
     * @param dbTypeEnum
     */
    public static void setDbType(DBTypeEnum dbTypeEnum) {
        contextHolder.set(dbTypeEnum.getValue());
    }

    /**
     * 取得当前数据源
     * @return
     */
    public static String getDbType() {
        return (String) contextHolder.get();
    }

    /**
     * 清除上下文数据
     */
    public static void clearDbType() {
        contextHolder.remove();
    }
}
