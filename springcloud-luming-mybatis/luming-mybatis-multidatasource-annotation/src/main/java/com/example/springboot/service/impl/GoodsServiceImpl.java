package com.example.springboot.service.impl;

import com.example.springboot.common.DBTypeEnum;
import com.example.springboot.config.DataSourceSwitch;
import com.example.springboot.mapper.DB3Dao.GoodsDao;
import com.example.springboot.pojo.Goods;
import com.example.springboot.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @DataSourceSwitch(DBTypeEnum.db3)
    @Override
    public List<Goods> getAllGoods() {
        return goodsDao.getAllGoods();
    }
}
