package com.example.springboot.mapper.DB2Dao;

import com.example.springboot.pojo.Book;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BookDao {

    List<Book> selectAllBook();
}
