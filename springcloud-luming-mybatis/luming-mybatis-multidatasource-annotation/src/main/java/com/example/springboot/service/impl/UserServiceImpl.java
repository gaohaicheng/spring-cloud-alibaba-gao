package com.example.springboot.service.impl;

import com.example.springboot.common.DBTypeEnum;
import com.example.springboot.config.DataSourceSwitch;
import com.example.springboot.mapper.DB1Dao.UserDao;
import com.example.springboot.pojo.User;
import com.example.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @DataSourceSwitch(DBTypeEnum.db1)
    @Override
    public List<User> getUsers() {
        return userDao.selectAllUser();
    }
}
