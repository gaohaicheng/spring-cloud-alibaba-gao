package com.luming.tkbatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import tk.mybatis.spring.annotation.MapperScan;
/**
 * 
 * @ClassName: SpringbootTkmybatisApplication
 * @Description: TODO
 * @author Luming
 * @date 2021-06-13 08:41:18
配置扫描文件
	第一种：在自己的Mapper类上添加 @Mapper注解
	第二种：在springboot启动类上，配置 @MapperScan("com.luming.tkbatis.mapper")  推荐第二种


 */
@SpringBootApplication
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) //禁用系统自动启用自动配置的额数据源 - 因为我们要自己定义数据源 所以要禁用
@MapperScan({"com.luming.tkbatis.mapper"})   //tk.mybatis.spring.annotation.MapperScan;
public class SpringbootTkmybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTkmybatisApplication.class, args);
    }
}