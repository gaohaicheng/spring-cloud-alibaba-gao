package com.luming.tkbatis.config;


import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.alibaba.druid.pool.DruidDataSource;
import com.github.pagehelper.PageHelper;

import lombok.Getter;
import lombok.Setter;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 * @ClassName: MybatisConfig
 * @Description: TODO
 * @author Luming
 * @date 2021-06-13 12:39:09
 * 	在这里要注意一下:MyBatisMapperScannerConfig 类的加载要在MybatisConfig 之后 ,否则报错
 
 	此config可以配置数据源， 并且扫描问题可以通过此配置实现，不需要再额外配置扫描 mapper类 mapper.xml 以及 com.luming.tkbatis.entity实体类扫描
 
 	想实现 mapper 继承自定义的BaseMapper<T> 必须要 使用此方式来实现
 	public interface UserMapper extends BaseTkMapper<User> {
 
 
 	Druid首先是一个数据库连接池。Druid是目前最好的数据库连接池，在功能、性能、扩展性方面，
 	都超过其他数据库连接池，包括DBCP、C3P0、BoneCP、Proxool、JBoss DataSource。
 
 
 	
 	《《《《 此配置  不涉及变更默认数据源， 配置 TK mybatis的自定义 Mapper  则不启用 》》》》
 
 
 */
@Getter
@Setter
//@Configuration
//@ConfigurationProperties(prefix="spring.datasource.db") //获取properties文件中的前缀属性字段
public class MybatisConfig {

	private String url;
    private String username;
    private String password;
	
	
    /**
     * 
		@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//禁用系统自动启用自动配置的额数据源 - 因为我们要自己定义数据源 所以要禁用     * 
     */
	@Bean
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        System.out.println("spring.datasource.db.url          :"+url);
        dataSource.setUrl(url);
        dataSource.setUsername(username);// 用户名
        dataSource.setPassword(password);// 密码
        return dataSource;
    }
	
	
//    @Resource   //默认的 dataSource 的驱动  #driver-class-name: com.mysql.cj.jdbc.Driver
//    private DataSource dataSource;

    @Bean
    public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource());

        //添加XML目录  注意: resource目录下必须要有mapper文件夹  否则报错,此处也是坑
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        bean.setMapperLocations(resolver.getResources("mappers/*.xml"));
        //扫描实体类所在包
        bean.setTypeAliasesPackage("com.luming.tkbatis.entity");
        return bean.getObject();
    }

	//注意此类的加载必须要在MybatisConfig之后在加载
//    @Configuration
//    @AutoConfigureAfter(MybatisConfig.class)
    public static class MyBatisMapperScannerConfig {

    	/**
    	 *  配置 tk mybaitis中的 自定义的 Mapper类
    	 * @MethodName: mapperScannerConfig
    	 * @Description: TODO
    	 * @author Luming
    	 * @return MapperScannerConfigurer
    	 * @date 2021-06-13 01:17:03
    	 */
        @Bean
        public MapperScannerConfigurer mapperScannerConfig() {
            MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
            mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactoryBean");
            //扫描mapper位置
            mapperScannerConfigurer.setBasePackage("com.luming.tkbatis.mapper");//"com.luming.tkbatis.modules.*.*"
            //配置通用mappers
            Properties properties = new Properties();
            //注册自定义mapper   BaseTkMapper这个是我们自己定义的通用mapper，它放置一个单独的包下面。也就是说这个包下面只有这个一个mapper，否则我们在实用泛型时会出现类型转换的问题。
            properties.setProperty("mappers", "com.luming.tkbatis.basemapper.BaseTkMapper");
            properties.setProperty("notEmpty", "false");
            properties.setProperty("IDENTITY", "MYSQL");
            mapperScannerConfigurer.setProperties(properties);

            return mapperScannerConfigurer;
        }

    }

    /**
     *	 配置mybatis的分页插件pageHelper
     * @return
     */
    @Bean
    public PageHelper pageHelper(){
        PageHelper pageHelper = new PageHelper();
        Properties properties = new Properties();
        properties.setProperty("offsetAsPageNum","true");
        properties.setProperty("rowBoundsWithCount","true");
        properties.setProperty("reasonable","true");
        //指定为MySQL数据库
        properties.setProperty("helperDialect","mysql");
        pageHelper.setProperties(properties);
        return pageHelper;
    }


    
}

