package com.luming.tkbatis.mapper;

import org.apache.ibatis.annotations.Select;

import com.luming.tkbatis.entity.User;

import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

/**
 *
 * @ClassName: UserMapper
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:14:29
 */
@Repository
public interface UserMapper extends Mapper<User> {

    User Sel( int id);

    /**
     Springboot整合tk.mybatis与注解方式共用
	  直接在Mapper中写就行
     */
    @Select( "select * from user where name = #{name}")
    User getUserByName (String name);





}
