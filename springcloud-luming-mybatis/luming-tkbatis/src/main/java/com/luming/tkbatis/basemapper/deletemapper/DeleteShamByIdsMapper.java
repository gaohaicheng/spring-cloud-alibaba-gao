package com.luming.tkbatis.basemapper.deletemapper;

import org.apache.ibatis.annotations.DeleteProvider;
/**
 * 
 * @ClassName: DeleteShamByIdsMapper
 * @Description: 创建DeleteShamByIdsMapper接口
 * @author Luming
 * @date 2021-06-13 10:49:56
 */
public interface DeleteShamByIdsMapper<T> {
    @DeleteProvider(
            type = IdsProviderDefined.class,
            method = "dynamicSQL"
    )
    int deleteShamByIds(String var1);
    //这里的抽象方法的名称必须和IdsProviderDefined 中的方法一致
}

