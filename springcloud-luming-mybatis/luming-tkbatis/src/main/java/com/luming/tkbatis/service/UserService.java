package com.luming.tkbatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luming.tkbatis.entity.User;
import com.luming.tkbatis.mapper.UserMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * user业务处理类
 * @ClassName: UserService
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:14:37
 */
@Service
public class UserService {

    @Autowired
    UserMapper userMapper;


    public User Sel(int id){
        return userMapper.Sel(id);
    }


    public List<User> queryUser(String name, boolean desc){
    	// 带条件查询方式
    	Example example = new Example(User.class);
    	example.createCriteria().orLike("name", "%"+ name + "%").orEqualTo("userName", name.toUpperCase());
    	String orderByClause = "name" + (desc ? " DESC" : " ASC");
    	example.setOrderByClause(orderByClause);
    	List<User> list = userMapper.selectByExample(example);
    	return list;
    }



    public User getUserByName(String name){
        return userMapper.getUserByName(name);
    }




}
