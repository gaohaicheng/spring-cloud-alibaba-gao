package com.luming.tkbatis.config;


import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

import lombok.Getter;
import lombok.Setter;

/**
 * 	读取外部的yaml文件放入系统中使用
 * @ClassName: SpringBootPropertiesConfig
 * @Description: TODO
 * @author Luming
 * @date 2021-06-13 01:32:58
 */
@Getter
@Setter
@Configuration
public class SpringBootPropertiesConfig {

	// 加载YML格式自定义配置文件
		@Bean
		public static PropertySourcesPlaceholderConfigurer properties() {
			PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
			YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
//			yaml.setResources(new FileSystemResource(ResourceUtils.CLASSPATH_URL_PREFIX + "permission.yml"));//File引入
			yaml.setResources(new ClassPathResource("application-druid.yaml"));//class引入，避免了路径处理问题
			configurer.setProperties(yaml.getObject());
			return configurer;
		}

    
}

