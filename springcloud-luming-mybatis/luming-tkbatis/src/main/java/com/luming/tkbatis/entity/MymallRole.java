package com.luming.tkbatis.entity;

import lombok.Data;

@Data
public class MymallRole extends BaseTkEntity{
	
	private int id;
	
	private String name;
	
	private String desc;
	
	private Boolean enabled; 
	
}