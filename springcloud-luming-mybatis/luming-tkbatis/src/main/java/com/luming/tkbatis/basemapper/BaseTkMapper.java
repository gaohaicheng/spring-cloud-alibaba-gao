package com.luming.tkbatis.basemapper;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author luming
 * @description: 增强mapper基类，支持批量操作，其他mapper继承此mapper
 * @date 2021/06/13 01:04

// 使用方法
	public List<Category> queryByIds(List<Long> ids){
	    List<Category> list = categoryMapper.selectByIdList(ids);
	    if (CollectionUtils.isEmpty(list)){
	        throw new LyException(ExceptionEnum.CATEGORY_NOT_FOUND);
	    }
	    return list;
	}

 */
public interface BaseTkMapper<T> extends Mapper<T>, MySqlMapper<T> , IdListMapper<T, Long>, InsertListMapper<T>{
}