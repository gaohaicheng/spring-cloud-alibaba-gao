package com.luming.tkbatis.common;

import cn.hutool.core.util.StrUtil;

/**
 * @author : hekang
 * @Description: 定义oss文件操作中使用的变量
 * @date :2020/11/24 11:04
 **/
public final class Constant {
    private Constant() {
        throw new UnsupportedOperationException();
    }

    /**
     * 阿里云API的内或外网域名
     */
    public static final String ENDPOINT = "oss-accelerate.aliyuncs.com";

    public static final String ENN_MAIL_SUFFIX = "@enn.cn";

    private static final String SYSTEM_USER = "system_user";

    public static String systemUserWith(String source) {
        if (StrUtil.isBlank(source)) {
            return SYSTEM_USER;
        }
        return SYSTEM_USER + "_" + source;
    }

    /**
     * OSS签名key
     */
    public static final String ACCESS_KEY_ID = "LTAI4Fw4awYr4xbhRhhWmkjS";

    /**
     * OSS签名密钥
     */
    public static final String ACCESS_KEY_SECRET = "eAOSOBqWq65lmn2TV7FffSWf5lD32T";

    /**
     * 存储空间名称
     */
    public static final String BUCKET_NAME = "app-develop-platform";

    public static final String JPG = ".jpg";
    public static final String PNG = ".png";
    public static final String B = "B";
    public static final String K = "K";
    public static final String M = "M";
    public static final String G = "G";

    public static final String BUSINESS_TOKEN = "customer-business-token";
    public static final String HEADER_OPERATOR = "operator";
    public static final String SYSTEM_USER_INFO = "system-user-info";

    public static final class BizCodeConstant {
        private BizCodeConstant() {
            throw new UnsupportedOperationException();
        }

        public static final String BIZ_CODE_NOT_CONFIG = "未配置bizCode";

        public static final String ENERGY = "energy";
        public static final String ENN = "enn";
        public static final String WATER = "water";
        public static final String ENNEW = "ennew";
        //创新者社区（成果中心）
        public static final String INNOVATOR = "innovator";
        //创新者社区-股份（成果中心）
        public static final String INNOVATOR_STOCK = "stock";
        //测试环境成果中心租户
        public static final String ACHIEVE = "achieve";
        //小程序租户code
        public static final String APPLET = "ecology-open-platform";
        //新绎控股
        public static final String XINYI = "xinyikonggu";

    }

    public static String POSITION_INT_EXPRESSION = "^[1-9]\\d*$";



}
