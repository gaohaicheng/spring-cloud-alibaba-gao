package com.luming.tkbatis.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * PO注解，定义字段占位符和默认名称
 *
 * @author songdebing
 * @date 2021/9/13
 */
@Target({TYPE, FIELD})
@Retention(RUNTIME)
public @interface Display {

    /**
     * 占位符
     **/
    String key();

    /**
     * 名称
     **/
    String value();

    /**
     * 如果是字段,是否默认当前字段拼上类名的前缀.
     */
    boolean autoPre() default false;
}
