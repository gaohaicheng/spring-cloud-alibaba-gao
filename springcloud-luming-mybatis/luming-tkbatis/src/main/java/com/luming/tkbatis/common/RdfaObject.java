package com.luming.tkbatis.common;

import java.io.Serializable;

public interface RdfaObject extends Serializable {
    String toLog();
}
