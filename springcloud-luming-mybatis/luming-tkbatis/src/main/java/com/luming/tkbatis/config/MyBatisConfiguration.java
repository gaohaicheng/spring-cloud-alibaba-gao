package com.luming.tkbatis.config;

import java.util.Properties;

import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.luming.tkbatis.basemapper.CommonMapper;

import tk.mybatis.spring.mapper.MapperScannerConfigurer;
/**

基于tk.mybatis扩展自己的通用mapper,将自己扩展的mapper添加到配置中。

	1>CommonMapper这个是我们自己定义的通用mapper，它放置一个单独的包下面。也就是说这个包下面只有这个一个mapper，否则我们在实用泛型时会出现类型转换的问题。（我在这里纠结了好久）
	2>这里我们配置了自定义mapper的扫描路径。注意springboot项目一定要注意jar包版本，版本过低的话，在配置类中的定义是没有用的。还是扫描不到，我之前使用的版本1.2.3就会出现这个问题，更换更高的版本就可以解决了。

 * @ClassName: MyBatisConfiguration
 * @Description: TODO
 * @author Luming
 * @date 2021-06-13 10:53:20
 */

//@Configuration
//@AutoConfigureAfter(MybatisAutoConfiguration.class)
public class MyBatisConfiguration {
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.example.dao.mapper");
        Properties properties = new Properties();
        //CommonMapper这个是我们自己定义的通用mapper
        properties.setProperty("mappers", CommonMapper.class.getName());
        properties.setProperty("notEmpty", "false");
        properties.setProperty("identity", "MYSQL");
        properties.setProperty("order","BEFORE");
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;
    }
}
