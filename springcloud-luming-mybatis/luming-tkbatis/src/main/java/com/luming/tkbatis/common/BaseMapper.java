package com.luming.tkbatis.common;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.additional.aggregation.AggregationMapper;
import tk.mybatis.mapper.additional.idlist.IdListMapper;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface BaseMapper<T> extends Mapper<T>, IdListMapper<T, Long>, AggregationMapper<T> {


    @Insert("create table ${tableName} like ${likeTableName}")
    void createTableLikeDate(@Param("tableName") String tableName, @Param("likeTableName") String likeTableName);

    /**
     * 自定义sql查询
     */
    @Select(value = "${sql}")
    List<Map<String, Object>> mapListBySql(@Param("sql") String sql);

    @Select(value = "${sql}")
    List<Object> objListBySql(@Param("sql") String sql);

    @Select(value = "${sql}")
    List<T> listBySql(@Param("sql") String sql);

    @Select(value = "${sql}")
    Object objBySql(@Param("sql") String sql);

    @Select("select count(1) from information_schema.TABLES where table_name = #{tableName}")
    int tableExist(@Param("tableName") String tableName);

}
