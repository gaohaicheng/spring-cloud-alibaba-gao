package com.luming.tkbatis.common;

import com.luming.tkbatis.annotation.ChangeLogField;
import com.luming.tkbatis.annotation.Display;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.annotation.LogicDelete;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author sunsulei
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public abstract class BaseDbPo extends BasePo {

    @Id
    @GeneratedValue(generator = "JDBC")
    @Display(key = "id", value = "ID", autoPre = true)
    private Long id;

    @LogicDelete
    @Display(key = "del", value = "删除标志", autoPre = true)
    private Boolean del = false;

    @Column(name = "biz_code", columnDefinition = "业务身份.任何 sql 操作需要判断当前操作用户携带的 bizCode")
    @ChangeLogField("业务身份")
    @Display(key = "bizCode", value = "业务身份", autoPre = true)
    private String bizCode;

    @Override
    public String toLog() {
        return this.toString();
    }
}
