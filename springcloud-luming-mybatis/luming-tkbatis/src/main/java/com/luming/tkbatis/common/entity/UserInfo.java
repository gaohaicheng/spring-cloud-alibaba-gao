//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.luming.tkbatis.common.entity;

import javax.validation.constraints.NotBlank;

import com.luming.tkbatis.common.RdfaObject;

public class UserInfo implements RdfaObject {
    @NotBlank
    private String userId;
    @NotBlank
    private String username;
    private String nickName;
    private Boolean hasTenant = false;
    private String tenantId = null;

    public UserInfo() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Boolean getHasTenant() {
        return this.hasTenant;
    }

    public void setHasTenant(Boolean hasTenant) {
        this.hasTenant = hasTenant;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String toLog() {
        return "UserInfo{userId='" + this.userId + '\'' + ", username='" + this.username + '\'' + ", nickName='" + this.nickName + '\'' + ", hasTenant=" + this.hasTenant + ", tenantId='" + this.tenantId + '\'' + '}';
    }

    public String toString() {
        return "UserInfo{userId='" + this.userId + '\'' + ", username='" + this.username + '\'' + ", nickName='" + this.nickName + '\'' + ", hasTenant=" + this.hasTenant + ", tenantId='" + this.tenantId + '\'' + '}';
    }
}
