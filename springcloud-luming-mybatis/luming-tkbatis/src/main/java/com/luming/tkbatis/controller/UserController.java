package com.luming.tkbatis.controller;
 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luming.tkbatis.entity.User;
import com.luming.tkbatis.service.UserService;
 
/**
 * 
 * @ClassName: UserController
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:14:21
 */
@RestController
@RequestMapping("/testBoot")
public class UserController {
 
    @Autowired
    private UserService userService;
 
    /**
     * http://localhost:8080/testBoot/getUser/1
     * @MethodName: GetUser
     * @Description: TODO
     * @author Luming
     * @param id
     * @return String
     * @date 2021-06-12 02:17:00
     */
    @RequestMapping("getUser/{id}")
    public String GetUser(@PathVariable int id){
        return userService.Sel(id).toString();
    }
    
    
    
    @RequestMapping("getUserByName/{name}")
    public String GetUserByName(@PathVariable String name){
        return userService.getUserByName(name).toString();
    }
    
    
    @RequestMapping("query/{name}")
    public Object query(@PathVariable String name){
    	List<User> queryUser = userService.queryUser(name,true);
        return queryUser;
    }
    
}
