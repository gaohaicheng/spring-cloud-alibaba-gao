package com.luming.tkbatis.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 	数据库实体对象
 * @ClassName: User
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:10:14
其中@Table即数据表表名，@Column即列名，@Id作为主键，需要注意，@Id注解不可有多个，@Transient即冗余字段，不与数据库任何字段对应。
注意多数据源的情况：则@Table注解中可以写成“{数据库名}.{架构名}.{表名}”，如：@Table(name=“db.dbo.tableName”)


原生mybatis 和 tkmybatis 均使用相同的持久化方式，去识别实体表名，属性和表列名对应

 */
@Data
@Table(name="user")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    private Integer id;
	
	@Column(name = "userName")  //插入数据时，数据和 表字段对应
    private String userName;
	
	@Column
    private String name;
	
	@Column(name = "passWord") 
    private String passWord;
	
	@Column(name = "realName") 
    private String realName;
 
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
        return id;
    }
 
    public void setId(Integer id) {
        this.id = id;
    }
 
    public String getUserName() {
        return userName;
    }
 
    public void setUserName(String userName) {
        this.userName = userName;
    }
 
    public String getPassWord() {
        return passWord;
    }
 
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
 
    public String getRealName() {
        return realName;
    }
 
    public void setRealName(String realName) {
        this.realName = realName;
    }
 
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", realName='" + realName + '\'' +
                '}';
    }
}
