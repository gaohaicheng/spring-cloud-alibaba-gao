package com.luming.tkbatis.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 变更日志字段
 *
 * @author sunsulei
 */
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface ChangeLogField {

    String value() default "未指定字段描述";
}
