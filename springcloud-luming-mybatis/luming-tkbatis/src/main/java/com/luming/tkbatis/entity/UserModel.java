package com.luming.tkbatis.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
/**
 * 	注意多数据源的情况：则@Table注解中可以写成“{数据库名}.{架构名}.{表名}”，如：@Table(name=“db.dbo.tableName”)
 * @ClassName: UserModel
 * @Description: TODO
 * @author Luming
 * @date 2021-06-13 12:31:55
 */
@Data
@Table(name="user_tk")
public class UserModel  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,generator = "JDBC")
    private Integer id;
    @Column
    private String userName;
    @Column
    private String userPhone;

}
