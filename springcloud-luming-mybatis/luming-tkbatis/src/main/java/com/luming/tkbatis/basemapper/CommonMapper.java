package com.luming.tkbatis.basemapper;

/**
 * @param <T>
 * @desc 基础增删改查功能mapper
 */
public interface CommonMapper<T> extends
        DeleteMapper<T>,
        InsertMapper<T>,
        SelectMapper<T>,
        UpdateMapper<T> {
}
