package com.luming.mybatis.mapper;
 
import org.springframework.stereotype.Repository;

import com.luming.mybatis.entity.User;
 
/**
 * 
 * @ClassName: UserMapper
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:14:29
 */
@Repository
public interface UserMapper {
 
    User Sel(int id);
}
