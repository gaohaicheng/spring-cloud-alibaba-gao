package com.luming.mybatis;
 
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
 /**
  * 
  * @ClassName: DataSource2Application
  * @Description: TODO
  * @author Luming
  * @date 2021-06-12 11:14:31
  
  方式2.JavaConfig方式配置数据源（更灵活）
 	启动类上注解
	@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//禁用系统自动启用自动配置的额数据源 - 因为我们要自己定义数据源 所以要禁用
  
  	Spring配置 Druid管理数据源：
  
  
我们自己定义 spring.datasource.db.XXX  	
  	
.properties 文件：
	
server.port=8080
# 数据库访问配置
spring.datasource.db.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.db.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.db.url=jdbc:mysql://localhost:3306/wallet_supper?useUnicode=true&characterEncoding=utf8&autoReconnect=true
spring.datasource.db.username=linpeng
spring.datasource.db.password=a123456
# 下面为连接池的补充设置，应用到上面所有数据源中
spring.datasource.db.initialSize=5
spring.datasource.db.minIdle=5
spring.datasource.db.maxActive=20
# 配置获取连接等待超时的时间
spring.datasource.db.maxWait=60000
# 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
spring.datasource.db.timeBetweenEvictionRunsMillis=60000
# 配置一个连接在池中最小生存的时间，单位是毫秒
spring.datasource.db.minEvictableIdleTimeMillis=300000
spring.datasource.db.validationQuery=SELECT 1 FROM DUAL
spring.datasource.db.testWhileIdle=true
spring.datasource.db.testOnBorrow=false
spring.datasource.db.testOnReturn=false
# 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
spring.datasource.db.filters=stat,wall,log4j
spring.datasource.db.logSlowSql=true

 指定数据源 DruidDataSource 在配置文件中完成。
  
  */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//禁用系统自动启用自动配置的额数据源 - 因为我们要自己定义数据源 所以要禁用
@EnableScheduling
public class DataSource2Application {
	public static void main(String[] args) {
 
		SpringApplication.run(DemoApplication.class, args);
		System.out.println("启动成功");
	}
}
