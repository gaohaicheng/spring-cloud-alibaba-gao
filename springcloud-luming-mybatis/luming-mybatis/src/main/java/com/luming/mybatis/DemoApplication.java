package com.luming.mybatis;
 
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 /**
  * 	下面在启动类里加上注解用于给出需要扫描的mapper文件路径@MapperScan("com.luming.mybatis.mapper") 
  * @ClassName: DemoApplication
  * @Description: TODO
  * @author Luming
  * @date 2021-06-12 02:15:29
  * 
  1、SpringBoot扫描Mybatis的mapper接口的两种方式
   		第一种就是在我们的mapper中加@mapper注解开启扫描
   		第二种就是在启动类加上@MapperScan(“com.example.dao”)则dao层不需要在加注解
   	
   	
   2、@MapperScan(basePackages = "com.xl.springboot.user.dao",annotationClass= Repository.class)
   		.扫描userDao，也就是我们的所说的各种mapper.java文件,此时注解是@Repository，和下面的一致
		启动类上添加 或者只mapper文件上加入@Mapper即可
   	
   	
   3、  最重点的是扫描mapper.xml文件，定位mapper.xml文件
   		mybatis.mapper-locations=classpath*:mappers/*.xml
   		为啥这个能扫描到呢？因为默认有这个



Mybatis数据源配置：
   
方式1.SpringBoot自动配置数据源（属性文件配置一下就可以了）
   	系统自带的数据源配置，使用yaml中的配置即可，
    spring:
  		datasource:
    		username: root
    		password: root
    		url: jdbc:mysql://192.168.1.21:3306/luming-mybatis?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC
    		driver-class-name: com.mysql.jdbc.Driver
 	SpringBoot的核心思维预定优于配置的思维
	自动配置内部已经约定了spring.datasource.xxx 只要我们配置赋值就可以了
 
 
方式2.JavaConfig方式配置数据源（更灵活）
 	启动类上注解
	@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})//禁用系统自动启用自动配置的额数据源 - 因为我们要自己定义数据源 所以要禁用
  	
  
  
   
  * 
  */
@MapperScan("com.luming.mybatis.mapper") //扫描的mapper
@SpringBootApplication
public class DemoApplication {
 
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
