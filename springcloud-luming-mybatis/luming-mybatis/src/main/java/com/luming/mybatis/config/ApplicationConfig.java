package com.luming.mybatis.config;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
  * 	此config配置是为了配置自定义的数据源所设置的。
  * @ClassName: ApplicationConfig
  * @Description: TODO
  * @author Luming
  * @date 2021-06-12 11:20:09

配合  DataSource2Application 启动类使用

  */

//@Configuration
//@ConfigurationProperties(prefix="spring.datasource.db") //获取properties文件中的前缀属性字段
public class ApplicationConfig {

    private String url;
    private String username;
    private String password;

    /**
     * @Bean 会把这个 return的dataSource数据源对象告诉Spring 去读取使用
     * @MethodName: getDataSource
     * @Description: TODO
     * @author Luming
     * @return DataSource
     * @date 2021-06-12 11:21:34
	也可以使用其他的数据源
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);// 用户名
        dataSource.setPassword(password);// 密码
        return dataSource;
	DataSource是一个数据源抽象类 详细可以查看
     */
    @Bean
    public DataSource getDataSource() {
       //AbstractRoutingDataSource
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);// 用户名
        dataSource.setPassword(password);// 密码
        return dataSource;
    }


    @Bean
    public DataSource dataSourceBean() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);// 用户名
        dataSource.setPassword(password);// 密码
        return dataSource;
    }


    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean sqlFactory = new SqlSessionFactoryBean();
        //SqlSessionFactory
        try {
            sqlFactory.setDataSource(getDataSource());
            //设置mybatis的主配置文件  记住是主配置文件啊
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource mybatisXml = resolver.getResource("classpath:mybatis-config.xml");
            sqlFactory.setConfigLocation(mybatisXml);
            //设置mapper.xml文件的路径
            Resource [] resource = resolver.getResources("classpath*:com/jiuzhm/memo/mybatis/**/*.xml");//{resourceMapXML};
            sqlFactory.setMapperLocations(resource);
            //扫描实体类所在包
            sqlFactory.setTypeAliasesPackage("com.jiuzhm.memo.mybatis.entity");
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return sqlFactory;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
