package com.luming.mybatis.service;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luming.mybatis.entity.User;
import com.luming.mybatis.mapper.UserMapper;
 
/**
 * user业务处理类
 * @ClassName: UserService
 * @Description: TODO
 * @author Luming
 * @date 2021-06-12 02:14:37
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;
    public User Sel(int id){
        return userMapper.Sel(id);
    }
}
