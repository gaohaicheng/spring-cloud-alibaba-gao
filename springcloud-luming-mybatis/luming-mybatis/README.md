# luming-mybatis

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
此项目为mybatis原生项目，引用mybatis原生jar

#### yaml配置文件说明

在项目中配置多套环境的配置方法。
因为现在一个项目有好多环境，开发环境，测试环境，准生产环境，生产环境，每个环境的参数不同，所以我们就可以把每个环境的参数配置到yml文件中，这样在想用哪个环境的时候只需要在主配置文件中将用的配置文件写上就行如application.yml

笔记：在Spring Boot中多环境配置文件名需要满足application-{profile}.yml的格式，其中{profile}对应你的环境标识，比如：
application-dev.yml：开发环境
application-test.yml：测试环境
application-prod.yml：生产环境
至于哪个具体的配置文件会被加载，需要在application.yml文件中通过spring.profiles.active属性来设置，其值对应{profile}值。



#### 启动类配置说明

  1、SpringBoot扫描Mybatis的mapper接口的两种方式
   		第一种就是在我们的mapper中加@mapper注解开启扫描
   		第二种就是在启动类加上@MapperScan(“com.example.dao”)则dao层不需要在加注解
   	
   	
   2、@MapperScan(basePackages = "com.xl.springboot.user.dao",annotationClass= Repository.class)
   		.扫描userDao，也就是我们的所说的各种mapper.java文件,此时注解是@Repository，和下面的一致
		启动类上添加 或者只mapper文件上加入@Mapper即可
   	
   	
   3、  最重点的是扫描mapper.xml文件，定位mapper.xml文件
   		
   		resources目录下定位：
   		
   		mybatis.mapper-locations=classpath*:mappers/*.xml
   		为啥这个能扫描到呢？pom中 因为默认有这个
        <build>
        <resources>
           <resource>
   		      <!--默认的--> 
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            </resources>
        </build>
        java包下的定位mapper.xml文件：
        mybatis.mapper-locations=classpath*:com/xl/springboot/**/*.xml


总结报错信息：

总结以及报错分析：
定位mapper.java文件：
尝试去掉annotationClass= Repository.class ，发明是没有问题的也就是说只要扫描包就行，需要具体定位可加注解
尝试去掉mapper.java文件中的@Repository 发现也是能够扫描到的,但是需要此注解交给spring进行管理,使@Autowired有效
尝试启动类不扫描包：换成mapper.java文件中的@Mapper注解，也是能正常获取到的

个人见解：@mapper 将接口生成对应的实现类，@Repository将实现类交给spring管理,具体如何实现需要匹配到xml文件

报错问题：
1.扫描mapper文件,如果没有报错：
    o.s.b.d.LoggingFailureAnalysisReporter   :
    Field userDao in com.xl.springboot.user.service.impl.LoginServiceImpl required a bean of type 'com.xl.springboot.user.dao.UserDao' that could not be found.

The injection point has the following annotations:
	- @org.springframework.beans.factory.annotation.Autowired(required=true)

2.交给spring管理，@Repository即可解决，如果没有@Autowired则编译不通过
3.建立xml与mapper文件的关联,如果没有会报错 Invalid bound statement (not found): com.xl.springboot.user.dao.UserDao.queryUserById




#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
