package com.maqh.demo.domain.reqment;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 需求表
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Reqment对象", description="需求表")
public class Reqment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "需求ID")
    private String reqmtId;

    @ApiModelProperty(value = "需求名称")
    private String reqmtTitle;

    @ApiModelProperty(value = "需求类型")
    private String reqmtType;

    @ApiModelProperty(value = "需求版本")
    private String reqmtVersion;

    @ApiModelProperty(value = "版本描述")
    private String versionDesc;

    @ApiModelProperty(value = "需求优先级1-非常紧急，2-紧急，3-普通，4-较低")
    private String reqmtLevel;

    @ApiModelProperty(value = "标签(字典表定义)")
    private String reqmtTags;

    @ApiModelProperty(value = "需求背景")
    private String reqmtBground;

    @ApiModelProperty(value = "目标价值")
    private String reqmtTagvalue;

    @ApiModelProperty(value = "功能场景")
    private String reqmtFunction;

    @ApiModelProperty(value = "需求提出时间")
    private LocalDateTime reqmtTime;

    @ApiModelProperty(value = "需求状态(字典表定义)0-待发布，1-已发布，2-设计中，3-已完成，4-已驳回，5-已删除,6-开发中，7-已发布,8-草稿")
    private String reqmtStatus;

    @ApiModelProperty(value = "需求人员ID")
    private String reqmtUserId;

    @ApiModelProperty(value = "需求人员名称")
    private String reqmtUser;

    @ApiModelProperty(value = "设计人员ID")
    private String designerId;

    @ApiModelProperty(value = "设计人员名称")
    private String designerName;

    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    @ApiModelProperty(value = "创建人名称")
    private String createUser;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    @ApiModelProperty(value = "更新人名称")
    private String updateUser;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除状态0-未删除，1-已删除")
    private String delStatus;


}
