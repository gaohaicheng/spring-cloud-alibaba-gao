package com.maqh.demo.controller.reqment;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.*;
import com.maqh.demo.service.reqment.IReqmentService;
import com.maqh.demo.domain.reqment.Reqment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 需求表 前端控制器
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
//@Api(tags = {"需求表"})
//@RestController
//@RequestMapping("//reqment")
public class ReqmentController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private IReqmentService reqmentService;

    @ApiOperation(value = "新增需求表")
    @PostMapping(value = "/init")
    public int init(@RequestBody Reqment reqment){
        return reqmentService.add(reqment);
    }

    @ApiOperation(value = "删除需求表")
    @DeleteMapping("{id}")
    public int delete(@PathVariable("id") Long id){
        return reqmentService.delete(id);
    }

    @ApiOperation(value = "id查询需求表")
    @GetMapping("{id}")
    public Reqment findById(@PathVariable Long id){
        return reqmentService.findById(id);
    }

}
