package com.maqh.demo.service.devicePressure;

import com.maqh.demo.domain.devicePressure.DevicePressureConf;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * <p>
 * 设备压力配置表 服务类
 * </p>
 *
 * @author maqh
 * @since 2022-05-10
 */
public interface IDevicePressureConfService extends IService<DevicePressureConf> {

    /**
     * 查询设备压力配置表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<DevicePressureConf>
     */
    IPage<DevicePressureConf> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加设备压力配置表
     *
     * @param devicePressureConf 设备压力配置表
     * @return int
     */
    int add(DevicePressureConf devicePressureConf);

    /**
     * 删除设备压力配置表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改设备压力配置表
     *
     * @param devicePressureConf 设备压力配置表
     * @return int
     */
    int updateData(DevicePressureConf devicePressureConf);

    /**
     * id查询数据
     *
     * @param id id
     * @return DevicePressureConf
     */
    DevicePressureConf findById(Long id);
}
