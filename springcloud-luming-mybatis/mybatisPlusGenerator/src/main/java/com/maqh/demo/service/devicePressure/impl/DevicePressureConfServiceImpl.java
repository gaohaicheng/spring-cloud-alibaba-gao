package com.maqh.demo.service.devicePressure.impl;

import com.maqh.demo.domain.devicePressure.DevicePressureConf;
import com.maqh.demo.dao.devicePressure.DevicePressureConfMapper;
import com.maqh.demo.service.devicePressure.IDevicePressureConfService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

/**
 * <p>
 * 设备压力配置表 服务实现类
 * </p>
 *
 * @author maqh
 * @since 2022-05-10
 */
@Service
public class DevicePressureConfServiceImpl extends ServiceImpl<DevicePressureConfMapper, DevicePressureConf> implements IDevicePressureConfService {

    @Override
    public  IPage<DevicePressureConf> findListByPage(Integer page, Integer pageCount){
        IPage<DevicePressureConf> wherePage = new Page<>(page, pageCount);
        DevicePressureConf where = new DevicePressureConf();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(DevicePressureConf devicePressureConf){
        return baseMapper.insert(devicePressureConf);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(DevicePressureConf devicePressureConf){
        return baseMapper.updateById(devicePressureConf);
    }

    @Override
    public DevicePressureConf findById(Long id){
        return  baseMapper.selectById(id);
    }
}
