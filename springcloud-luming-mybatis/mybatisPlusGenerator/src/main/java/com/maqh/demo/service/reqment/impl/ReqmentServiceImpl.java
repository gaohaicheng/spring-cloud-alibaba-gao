package com.maqh.demo.service.reqment.impl;

import com.maqh.demo.domain.reqment.Reqment;
import com.maqh.demo.dao.reqment.ReqmentMapper;
import com.maqh.demo.service.reqment.IReqmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

/**
 * <p>
 * 需求表 服务实现类
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
@Service
public class ReqmentServiceImpl extends ServiceImpl<ReqmentMapper, Reqment> implements IReqmentService {

    @Override
    public  IPage<Reqment> findListByPage(Integer page, Integer pageCount){
        IPage<Reqment> wherePage = new Page<>(page, pageCount);
        Reqment where = new Reqment();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(Reqment reqment){
        return baseMapper.insert(reqment);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(Reqment reqment){
        return baseMapper.updateById(reqment);
    }

    @Override
    public Reqment findById(Long id){
        return  baseMapper.selectById(id);
    }
}
