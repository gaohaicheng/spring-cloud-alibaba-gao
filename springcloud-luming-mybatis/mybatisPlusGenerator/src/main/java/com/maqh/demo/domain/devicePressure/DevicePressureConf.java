package com.maqh.demo.domain.devicePressure;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 设备压力配置表
 * </p>
 *
 * @author maqh
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="DevicePressureConf对象", description="设备压力配置表")
public class DevicePressureConf implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "设备编号")
    private String dn;

    @ApiModelProperty(value = "运行压力")
    private Double pa;

    @ApiModelProperty(value = "放散压力")
    private Double rp;

    @ApiModelProperty(value = "切断压力")
    private Double cop;

    @ApiModelProperty(value = "创建时间")
    private Date cdt;


}
