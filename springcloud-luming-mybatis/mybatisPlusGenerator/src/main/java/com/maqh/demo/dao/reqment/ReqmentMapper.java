package com.maqh.demo.dao.reqment;

import com.maqh.demo.domain.reqment.Reqment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 需求表 Mapper 接口
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
public interface ReqmentMapper extends BaseMapper<Reqment> {

}
