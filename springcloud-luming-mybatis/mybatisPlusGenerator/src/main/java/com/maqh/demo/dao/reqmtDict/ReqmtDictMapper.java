package com.maqh.demo.dao.reqmtDict;

import com.maqh.demo.domain.reqmtDict.ReqmtDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典管理表字段 Mapper 接口
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
public interface ReqmtDictMapper extends BaseMapper<ReqmtDict> {

}
