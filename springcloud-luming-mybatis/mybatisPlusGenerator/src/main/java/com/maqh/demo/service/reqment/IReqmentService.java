package com.maqh.demo.service.reqment;

import com.maqh.demo.domain.reqment.Reqment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * <p>
 * 需求表 服务类
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
public interface IReqmentService extends IService<Reqment> {

    /**
     * 查询需求表分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<Reqment>
     */
    IPage<Reqment> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加需求表
     *
     * @param reqment 需求表
     * @return int
     */
    int add(Reqment reqment);

    /**
     * 删除需求表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改需求表
     *
     * @param reqment 需求表
     * @return int
     */
    int updateData(Reqment reqment);

    /**
     * id查询数据
     *
     * @param id id
     * @return Reqment
     */
    Reqment findById(Long id);
}
