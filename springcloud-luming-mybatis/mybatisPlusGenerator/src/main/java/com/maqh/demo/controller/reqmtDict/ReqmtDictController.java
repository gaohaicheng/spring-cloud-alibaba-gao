package com.maqh.demo.controller.reqmtDict;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.*;
import com.maqh.demo.service.reqmtDict.IReqmtDictService;
import com.maqh.demo.domain.reqmtDict.ReqmtDict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 字典管理表字段 前端控制器
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
//@Api(tags = {"字典管理表字段"})
//@RestController
//@RequestMapping("/reqmt-dict")
public class ReqmtDictController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private IReqmtDictService reqmtDictService;


    @ApiOperation(value = "新增字典管理表字段")
    @PostMapping()
    public int add(@RequestBody ReqmtDict reqmtDict){
        return reqmtDictService.add(reqmtDict);
    }

    @ApiOperation(value = "删除字典管理表字段")
    @DeleteMapping("{id}")
    public int delete(@PathVariable("id") Long id){
        return reqmtDictService.delete(id);
    }

    @ApiOperation(value = "更新字典管理表字段")
    @PutMapping()
    public int update(@RequestBody ReqmtDict reqmtDict){
        return reqmtDictService.updateData(reqmtDict);
    }

    @ApiOperation(value = "查询字典管理表字段分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping()
    public IPage<ReqmtDict> findListByPage(@RequestParam Integer page,
                                   @RequestParam Integer pageCount){
        return reqmtDictService.findListByPage(page, pageCount);
    }

    @ApiOperation(value = "id查询字典管理表字段")
    @GetMapping("{id}")
    public ReqmtDict findById(@PathVariable Long id){
        return reqmtDictService.findById(id);
    }

}
