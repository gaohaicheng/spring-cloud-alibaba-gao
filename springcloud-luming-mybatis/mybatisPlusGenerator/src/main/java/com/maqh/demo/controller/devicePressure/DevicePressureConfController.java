package com.maqh.demo.controller.devicePressure;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.web.bind.annotation.*;
import com.maqh.demo.service.devicePressure.IDevicePressureConfService;
import com.maqh.demo.domain.devicePressure.DevicePressureConf;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 设备压力配置表 前端控制器
 * </p>
 *
 * @author maqh
 * @since 2022-05-10
 */
//@Api(tags = {"设备压力配置表"})
//@RestController
//@RequestMapping("//device-pressure-conf")
public class DevicePressureConfController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private IDevicePressureConfService devicePressureConfService;


    @ApiOperation(value = "新增设备压力配置表")
    @PostMapping()
    public int add(@RequestBody DevicePressureConf devicePressureConf){
        return devicePressureConfService.add(devicePressureConf);
    }

    @ApiOperation(value = "删除设备压力配置表")
    @DeleteMapping("{id}")
    public int delete(@PathVariable("id") Long id){
        return devicePressureConfService.delete(id);
    }

    @ApiOperation(value = "更新设备压力配置表")
    @PutMapping()
    public int update(@RequestBody DevicePressureConf devicePressureConf){
        return devicePressureConfService.updateData(devicePressureConf);
    }

    @ApiOperation(value = "查询设备压力配置表分页数据")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", value = "页码"),
        @ApiImplicitParam(name = "pageCount", value = "每页条数")
    })
    @GetMapping()
    public IPage<DevicePressureConf> findListByPage(@RequestParam Integer page,
                                   @RequestParam Integer pageCount){
        return devicePressureConfService.findListByPage(page, pageCount);
    }

    @ApiOperation(value = "id查询设备压力配置表")
    @GetMapping("{id}")
    public DevicePressureConf findById(@PathVariable Long id){
        return devicePressureConfService.findById(id);
    }

}
