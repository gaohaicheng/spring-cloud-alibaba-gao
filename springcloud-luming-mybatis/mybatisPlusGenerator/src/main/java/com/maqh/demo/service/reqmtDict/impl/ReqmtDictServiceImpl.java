package com.maqh.demo.service.reqmtDict.impl;

import com.maqh.demo.domain.reqmtDict.ReqmtDict;
import com.maqh.demo.dao.reqmtDict.ReqmtDictMapper;
import com.maqh.demo.service.reqmtDict.IReqmtDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

/**
 * <p>
 * 字典管理表字段 服务实现类
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
@Service
public class ReqmtDictServiceImpl extends ServiceImpl<ReqmtDictMapper, ReqmtDict> implements IReqmtDictService {

    @Override
    public  IPage<ReqmtDict> findListByPage(Integer page, Integer pageCount){
        IPage<ReqmtDict> wherePage = new Page<>(page, pageCount);
        ReqmtDict where = new ReqmtDict();

        return   baseMapper.selectPage(wherePage, Wrappers.query(where));
    }

    @Override
    public int add(ReqmtDict reqmtDict){
        return baseMapper.insert(reqmtDict);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(ReqmtDict reqmtDict){
        return baseMapper.updateById(reqmtDict);
    }

    @Override
    public ReqmtDict findById(Long id){
        return  baseMapper.selectById(id);
    }
}
