package com.maqh.demo.service.reqmtDict;

import com.maqh.demo.domain.reqmtDict.ReqmtDict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * <p>
 * 字典管理表字段 服务类
 * </p>
 *
 * @author maqh
 * @since 2022-03-03
 */
public interface IReqmtDictService extends IService<ReqmtDict> {

    /**
     * 查询字典管理表字段分页数据
     *
     * @param page      页码
     * @param pageCount 每页条数
     * @return IPage<ReqmtDict>
     */
    IPage<ReqmtDict> findListByPage(Integer page, Integer pageCount);

    /**
     * 添加字典管理表字段
     *
     * @param reqmtDict 字典管理表字段
     * @return int
     */
    int add(ReqmtDict reqmtDict);

    /**
     * 删除字典管理表字段
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改字典管理表字段
     *
     * @param reqmtDict 字典管理表字段
     * @return int
     */
    int updateData(ReqmtDict reqmtDict);

    /**
     * id查询数据
     *
     * @param id id
     * @return ReqmtDict
     */
    ReqmtDict findById(Long id);
}
