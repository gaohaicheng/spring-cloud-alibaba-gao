package com.maqh.demo.dao.devicePressure;

import com.maqh.demo.domain.devicePressure.DevicePressureConf;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备压力配置表 Mapper 接口
 * </p>
 *
 * @author maqh
 * @since 2022-05-10
 */
public interface DevicePressureConfMapper extends BaseMapper<DevicePressureConf> {


}
