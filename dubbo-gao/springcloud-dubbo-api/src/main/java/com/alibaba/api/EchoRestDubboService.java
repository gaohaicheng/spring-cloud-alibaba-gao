package com.alibaba.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("nacos-dubbo-cloud-provider-service")
public interface EchoRestDubboService {

    @GetMapping("/echoRestDubbo")
    String echo(@RequestParam String name);
}	