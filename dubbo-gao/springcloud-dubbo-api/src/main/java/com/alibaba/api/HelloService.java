package com.alibaba.api;
/**
* @ClassName: HelloService  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author hcgao  
* @date 2021年5月25日  
 */
public interface HelloService {
	
	String hello(String name);

}
