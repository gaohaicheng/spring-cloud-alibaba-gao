package com.alibaba.gao.cache;

import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.api.EchoRestDubboService;


@DubboService
public class EchoRestDubboServiceImpl implements EchoRestDubboService {

    private final Logger logger = LoggerFactory.getLogger(EchoRestDubboServiceImpl.class);

    @Override
    public String echo(String name) {

        logger.info("echo rest dubbo :{}", name);
        return "hello " + name;
    }
}
