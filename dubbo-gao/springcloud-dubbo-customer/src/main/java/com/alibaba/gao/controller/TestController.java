package com.alibaba.gao.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.api.EchoRestDubboService;

@RestController
public class TestController {
  
    @DubboReference
    private EchoRestDubboService echoDubboService;

//    @Autowired
//    private EchoRestDubboService echoRestService;

    @GetMapping("/echoDubbo")
    String echoDubbo(String name) {
        return echoDubboService.echo(name);
    }

//    @GetMapping("/echoRest")
//    String echoRest(String name) {
//        return echoRestService.echo(name);
//    }

}