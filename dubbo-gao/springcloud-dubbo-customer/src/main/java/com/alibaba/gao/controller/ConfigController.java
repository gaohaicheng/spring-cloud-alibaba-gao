package com.alibaba.gao.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.api.HelloService;

import io.swagger.annotations.ApiOperation;

@RefreshScope   // nacos配置中心修改后，配置可以同步更新
@RestController
@RequestMapping("/config")
public class ConfigController {
  
    @DubboReference
    private HelloService helloService;

    @Value("${config.info:}")
    private String config;

    
    //@RequestMapping(value = "/per-addPosition", method = RequestMethod.POST)
    @ApiOperation(value = "hello的返回")
    @GetMapping("/hello")
    String hello(@RequestParam String name) {
        return helloService.hello(name+" ---  config : " + config);
    }

//    @GetMapping("/echoRest")
//    String echoRest(String name) {
//        return echoRestService.echo(name);
//    }

}