package com.alibaba.gao.config;
 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.common.collect.Sets;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
 
/**
 * 
* @ClassName: SwaggerConfig  
* @Description: TODO(这里用一句话描述这个类的作用)  http://127.0.0.1:8081/swagger-ui.htm
* @author hcgao  
* @date 2021年5月25日  
*
 */
@Configuration
@EnableSwagger2WebMvc
//@ComponentScan(basePackages = "com.alibaba.gao")
public class SwaggerConfig extends WebMvcConfigurationSupport {
    @Bean
    public Docket commonDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("通用API接口文档")
                .apiInfo(apiInfo("提供通用接口"))
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))  //RequestHandlerSelectors.basePackage("pers.graduation.controller.common")
                .paths(PathSelectors.any())
                .build();
    }
 
    @Bean
    public Docket normalUserDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("普通用户API文档")
                .apiInfo(apiInfo("提供普通用户接口"))
                .protocols(Sets.newHashSet("https","http"))
                .produces(Sets.newHashSet("html/text"))
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("pers.graduation.controller.candidate"))//设置生成的Docket对应的Controller为candidate下的所有Controller
                .paths(PathSelectors.any())
                .build();
    }
 
    @Bean
    public Docket companyUserDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("企业用户API接口文档")
                .apiInfo(apiInfo("提供企业用户接口"))
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("pers.graduation.controller.company"))
                .paths(PathSelectors.any())
                .build();
    }
	//设置文档信息
    private ApiInfo apiInfo(String desc) {
        return new ApiInfoBuilder()
        		.title("cloud-alibaba")				//标题名称
        		.description("springCloudAlibaba")  //文档描述
        		.contact(contact())					//联系方式实体类
        		.version("v1.0")					//版本号，自定义
        		.build();

    }
	//设置联系方式
    private Contact contact() {
        return new Contact("CloudAlibaba", "http://blog.csdn.net", "15662121826@163.com");
    }
}
