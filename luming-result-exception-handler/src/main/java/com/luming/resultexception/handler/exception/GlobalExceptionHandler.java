package com.luming.resultexception.handler.exception;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

import com.luming.resultexception.handler.result.Result;
import com.luming.resultexception.handler.result.ResultCodeEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: GlobalExceptionHandler
 * @Description: TODO
 * @author Luming
 * @date 2021-06-14 10:42:39
统一异常处理
使用统一返回结果时，还有一种情况，就是程序的保存是由于运行时异常导致的结果，有些异常我们可以无法提前预知，不能正常走到我们return的R对象返回。
@ControllerAdvice
该注解为统一异常处理的核心
是一种作用于控制层的切面通知（Advice），该注解能够将通用的@ExceptionHandler、@InitBinder和@ModelAttributes方法收集到一个类型，并应用到所有控制器上
该类中的设计思路：
使用@ExceptionHandler注解捕获指定或自定义的异常；
使用@ControllerAdvice集成@ExceptionHandler的方法到一个类中；
必须定义一个通用的异常捕获方法，便于捕获未定义的异常信息；
自定一个异常类，捕获针对项目或业务的异常;
异常的对象信息补充到统一结果枚举中；
 */

//@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**-------- 通用异常处理方法 --------**/
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        // 通用异常结果打印到日志
        log.error(e.getMessage());
        return Result.error();
    }

    /**-------- 指定异常处理方法 --------**/
    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public Result error(NullPointerException e) {
        e.printStackTrace();
        return Result.setResult(ResultCodeEnum.NULL_POINT);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseBody
    public Result error(IndexOutOfBoundsException e) {
        e.printStackTrace();
        return Result.setResult(ResultCodeEnum.HTTP_CLIENT_ERROR);
    }

    /**-------- 自定义定异常处理方法 --------**/
    @ExceptionHandler(CMSException.class)
    @ResponseBody
    public Result error(CMSException e) {
        e.printStackTrace();
        return Result.error().message(e.getMessage()).code(e.getCode());
    }
}