package com.luming.resultexception.handler.result;

import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @ClassName: Result
 * @Description: TODO
 * @author Luming
 * @date 2021-06-14 10:42:13
统一结果类
外界只可以调用统一返回类的方法，不可以直接创建，因此构造器私有；
内置静态方法，返回对象；
为便于自定义统一结果的信息，建议使用链式编程，将返回对象设类本身，即return this;
响应数据由于为json格式，可定义为JsonObject或Map形式
 */
@Data
@ApiModel(value = "全局统一返回结果")
public class Result {
 
	@ApiModelProperty(value = "是否成功")
	private Boolean success;
 
	@ApiModelProperty(value = "返回码")
	private Integer code;
 
	@ApiModelProperty(value = "返回消息")
	private String message;
 
	@ApiModelProperty(value = "返回数据")
	private Map<String, Object> data = new HashMap<String, Object>();
 
	private Result(){}
     
	public static Result  ok(){
		Result  r = new Result ();
		r.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
		r.setCode(ResultCodeEnum.SUCCESS.getCode());
		r.setMessage(ResultCodeEnum.SUCCESS.getMessage());
		return r;
	}
 
	public static Result  error(){
		Result  r = new Result ();
		r.setSuccess(ResultCodeEnum.UNKNOWN_REASON.getSuccess());
		r.setCode(ResultCodeEnum.UNKNOWN_REASON.getCode());
		r.setMessage(ResultCodeEnum.UNKNOWN_REASON.getMessage());
		return r;
	}
 
	public static Result setResult(ResultCodeEnum resultCodeEnum){
		Result  r = new Result ();
		r.setSuccess(resultCodeEnum.getSuccess());
		r.setCode(resultCodeEnum.getCode());
		r.setMessage(resultCodeEnum.getMessage());
		return r;
	}
 
	public Result success(Boolean success){
		this.setSuccess(success);
		return this;
	}
 
	public Result message(String message){
		this.setMessage(message);
		return this;
	}
 
	public Result code(Integer code){
		this.setCode(code);
		return this;
	}
 
	public Result data(String key, Object value){
		this.data.put(key, value);
		return this;
	}
 
	public Result data(Map<String, Object> map){
		this.setData(map);
		return this;
	}
}
