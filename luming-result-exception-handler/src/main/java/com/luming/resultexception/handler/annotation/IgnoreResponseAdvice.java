package com.luming.resultexception.handler.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 	统一返回包装标识注解
 *	不加该注解默认处理统一异常
 *  GlobalDefaultExceptionHandler#errorDispose()   中处理该注解的作用
 * @author <a href="mailto:yaoonlyi@gmail.com">purgeyao</a>
 * @since 1.0.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreResponseAdvice {

    /**
     * 	是否进行全局异常处理封装
     * @return true:进行处理;  false:不进行异常处理  
     */
    boolean errorDispose() default true;

}
