package com.alibaba.gao.study.service;

import com.alibaba.gao.study.model.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 154594742@qq.com
 */
public interface UserService extends IService<UserEntity> {
}
