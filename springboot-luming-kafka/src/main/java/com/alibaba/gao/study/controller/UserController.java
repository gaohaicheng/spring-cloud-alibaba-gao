package com.alibaba.gao.study.controller;

import com.alibaba.gao.study.model.entity.UserEntity;
import com.alibaba.gao.study.model.param.UserParam;
import com.alibaba.gao.study.model.vo.ResponseVo;
import com.alibaba.gao.study.service.UserService;
import com.alibaba.gao.study.util.BuildResponseUtils;
import com.alibaba.gao.study.util.CommonQueryPageUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 用户控制器（一个demo用于大家学习参考）
 *
 * @author 154594742@qq.com
 * @date: 2021/2/22 10:02:00
 */

@RestController
@Api(tags = "用户控制器")
public class UserController {

    @Resource
    private UserService userServiceImpl;

    /**
     * 新增
     * @param entity 实体
     * @return ResponseVo
     */
    @ApiOperation("新增")
    @PostMapping("user")
    public ResponseVo<?> add(@RequestBody UserEntity entity) {
        return userServiceImpl.save(entity) ? BuildResponseUtils.success() : BuildResponseUtils.error();
    }

    /**
     * 通过id查询
     *
     * @param id 自增主键
     * @return ResponseVo
     */
    @ApiOperation("通过id查询")
    @GetMapping("user/{id}")
    public ResponseVo<UserEntity> getById(@PathVariable String id) {
        return BuildResponseUtils.buildResponse(userServiceImpl.getById(id));
    }

    /**
     * 更新
     *
     * @param entity 实体
     * @return ResponseVo
     */
    @ApiOperation("更新")
    @PutMapping("user")
    public ResponseVo<?> update(@RequestBody UserEntity entity) {
        return userServiceImpl.updateById(entity) ? BuildResponseUtils.success() : BuildResponseUtils.error();
    }

    /**
     * 通过id删除
     *
     * @param id 自增主键
     * @return ResponseVo
     */
    @ApiOperation("通过id删除")
    @DeleteMapping("user/{id}")
    public ResponseVo<?> delete(@PathVariable String id) {
        return userServiceImpl.removeById(id) ? BuildResponseUtils.success() : BuildResponseUtils.error();
    }

    /**
     * 分页查询
     *
     * @param param 请求参数
     * @return ResponseVo
     */
    @ApiOperation("分页查询")
    @GetMapping("userPage")
    public ResponseVo<IPage<UserEntity>> selectPage(@RequestBody UserParam param) {
        return BuildResponseUtils.buildResponse(CommonQueryPageUtils.commonQueryPage(param, userServiceImpl));
    }

}
