package com.alibaba.gao.study.mapper;

import com.alibaba.gao.study.model.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.mapstruct.Mapper;

/**
 * @author 154594742@qq.com
 */
public interface UserMapper extends BaseMapper<UserEntity> {

    public void queryMyData(String name);

}
