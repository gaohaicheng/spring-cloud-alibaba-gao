package com.alibaba.gao;

import com.alibaba.fastjson.JSON;
import com.alibaba.gao.pojo.User;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilderException;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Es 7.6.1 高级客户端测试
 */
@SpringBootTest
class SpringcloudLumingEsApiApplicationTests {


    @Autowired
    RestHighLevelClient restHighLevelClient;

    /**
     * 1、创建索引   类似 PUT gao_index
     */
    @Test
    void testCreateIndex() {
        // 1、 创建索引请求
        CreateIndexRequest indexRequest = new CreateIndexRequest("gao_index");
        try {
            //2、 客户端执行请求  IndicesClient，请求后获得响应
            CreateIndexResponse indexResponse = restHighLevelClient.indices().create(indexRequest, RequestOptions.DEFAULT);

            System.out.println(indexResponse);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    /**
     * 2、索引是否存在
     */
    @Test
    void testExistIndex() {
        // 1、 获取索引请求
        GetIndexRequest request = new GetIndexRequest("gao_index");
        try {
            //2、 客户端执行请求  IndicesClient，请求后获得响应
            boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
            System.out.println(exists);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    /**
     * 3、删除索引
     */
    @Test
    void testDeleteIndex() {
        // 1、 获取索引请求
        DeleteIndexRequest request = new DeleteIndexRequest("gao_index");
        try {
            //2、 客户端执行请求  IndicesClient，请求后获得响应
            AcknowledgedResponse response = restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);
            System.out.println(response.isAcknowledged());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }






    /**
     * 4、文档 添加
     */
    @Test
    void testAddDocument() throws IOException {

        //1 添加对象
        User user = new User("高海成", 30);
        //2 创建请求
        IndexRequest request = new IndexRequest("gao_index");

        //规则 put /gao_index/_doc/1
        request.id("1");
        request.timeout("1s");   // 或者 request.timeout(TimeValue.timeValueSeconds(1));

        //将数据放入请求  json
        request.source(JSON.toJSONString(user), XContentType.JSON);

        //客户端发送请求  获取响应结果
        IndexResponse response = restHighLevelClient.index(request, RequestOptions.DEFAULT);

        System.out.println(response.toString());
        System.out.println(response.status());  // 对应我们的命令返回状态   CREATED
    }




    /**
     * 5、文档 获取  判断存在  GET  /index/doc/1
     */
    @Test
    void testExistDocument() throws IOException {

        //2 创建请求
        GetRequest request = new GetRequest("gao_index", "1");
        // 不获取返回的 _source 上下文了
        request.fetchSourceContext(new FetchSourceContext(false));
        request.storedFields("_none_");
        boolean exists = restHighLevelClient.exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }


    /**
     * 6、文档 获取文档信息   判断存在后获取信息  GET  /index/doc/1
     */
    @Test
    void testGetDocument() throws IOException {

        //创建获取请求
        GetRequest request = new GetRequest("gao_index", "1");
        GetResponse response = restHighLevelClient.get(request, RequestOptions.DEFAULT);

        // 打印文档内容  只是我们存入的User 内容
        System.out.println(response.getSourceAsString());
        //返回全部内容和命令返回一样  GET  /index/doc/1
        System.out.println(response.toString());
    }




    /**
     * 7、文档 更新文档信息   POST  /index/doc/1
     */
    @Test
    void testUpdateDocument() throws IOException {

        //更新请求
        UpdateRequest request = new UpdateRequest("gao_index", "1");
        request.timeout("1s");

        User user = new User("高海成1", 31);
        request.doc(JSON.toJSONString(user), XContentType.JSON);


        UpdateResponse response = restHighLevelClient.update(request, RequestOptions.DEFAULT);

        // 获取更后状态
        System.out.println(response.status());
        System.out.println(response);
    }

    /**
     * 8、文档 删除   DELETE  /index/doc/1
     */
    @Test
    void testDeleteDocument() throws IOException {

        //删除请求
        DeleteRequest request = new DeleteRequest("gao_index", "1");
        request.timeout("1s");

        DeleteResponse response = restHighLevelClient.delete(request, RequestOptions.DEFAULT);

        // 获取更后状态
        System.out.println(response.status());
        System.out.println(response);
    }



    /**
     * 8、文档 真实项目中批量插入
     */
    @Test
    void testBulkRequest() throws IOException {

        //请求
        BulkRequest request = new BulkRequest();
        request.timeout("10s");

        ArrayList<User> userList = new ArrayList<>();
        userList.add(new User("高海成11", 22));
        userList.add(new User("高海成22", 24));
        userList.add(new User("高海成33", 21));
        userList.add(new User("高海成44", 23));
        userList.add(new User("高海成55", 26));


        // 批处理请求，  可以批量增加  删除  更新
        for (int i = 0; i < userList.size(); i++) {
            request.add(
                new IndexRequest("gao_index")
                        .id(""+(i+3))   // id不指定，会默认随机生成
                        .source(JSON.toJSONString(userList.get(i)), XContentType.JSON)
            );
        }

        BulkResponse bulkResponse = restHighLevelClient.bulk(request, RequestOptions.DEFAULT);

        // 获取状态
        System.out.println(bulkResponse.hasFailures()); // 是否失败
    }





    /**
     * 8、文档 真实项目中 查询 search
     */
    @Test
    void testSearch() throws IOException {

        //搜索请求
        SearchRequest searchRequest = new SearchRequest("gao_index");  //指定索引库，建议配置到常量中，或者配置文件中
        //构建搜索条件  分页  高亮 条件查询  排序等
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //构建高亮  highlightBuilder
        //sourceBuilder.highlighter();

        //构建 QueryBuilders 查询条件  精确查询 模糊查询 boolean查询等
        // QueryBuilders.termQuery 精确匹配
        //QueryBuilders.matchAllQuery 匹配所有
        //MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "高海成");
        sourceBuilder.query(termQueryBuilder);
        //构建分页
        //sourceBuilder.from();
        //sourceBuilder.size();

        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(sourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = searchResponse.getHits();

        // 获取查询结果
        System.out.println(JSON.toJSONString(hits)); //  hits 信息；

        System.out.println("============================================");

        SearchHit[] hitsHits = hits.getHits();
        for (SearchHit hitsHit : hitsHits) {
            System.out.println(hitsHit.getSourceAsMap());
        }
        System.out.println();


    }




    @Test
    void contextLoads() {

    }

}
