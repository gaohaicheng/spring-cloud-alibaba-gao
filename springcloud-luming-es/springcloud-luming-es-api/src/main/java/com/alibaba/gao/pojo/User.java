package com.alibaba.gao.pojo;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName User.java
 * @Description TODO
 * @createTime 2021年08月29日 12:22:00
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

    private String name;

    private int age;

}
