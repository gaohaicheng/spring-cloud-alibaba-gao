package com.alibaba.gao.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName ElasticsearchClientConfig.java
 * @Description TODO
 * @createTime 2021年08月29日 09:11:00
 *
 *
 *
 *   线分析源码 SpringBoot
 *   XXXAutoConfiguration   XXXProperties
 *
 */
@Configuration
public class ElasticsearchClientConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient(){
       /* RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                new HttpHost("localhost", 9200, "http"),
                new HttpHost("localhost", 9201, "http")));  集群*/
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
        return client;
    }

}
