package com.alibaba.gao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName JdContent.java
 * @Description TODO
 * @createTime 2021年08月30日 22:13:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JdContent {
    private String title;
    private String img;
    private String price;
}
