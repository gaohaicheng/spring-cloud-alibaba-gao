package com.alibaba.gao.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName IndexController.java
 * @Description TODO
 * @createTime 2021年08月30日 08:26:00
 */
// @RestController   写此注解，spring-boot-starter-thymeleaf 渲染页面不生效
//注意的是用@controller注解不要用@restController，前者是渲染页面用的，后者是返回数据用的。贴上代码
@Controller
public class IndexController {

    @GetMapping({"/","/index"})
    public String index() {
        return "index";
    }


    @GetMapping({"/test"})
    public String test() {
        return "test";
    }
}
