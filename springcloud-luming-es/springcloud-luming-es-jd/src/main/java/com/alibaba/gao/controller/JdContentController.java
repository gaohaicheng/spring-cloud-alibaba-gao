package com.alibaba.gao.controller;

import com.alibaba.gao.cache.JdContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName JdContentController.java
 * @Description TODO
 * @createTime 2021年08月30日 22:31:00
 */
@RestController
public class JdContentController {

    @Autowired
    private JdContentService jdContentService;

    @GetMapping("/parse/{keywords}")
    public String parseContent(@PathVariable(value = "keywords")  String keywords){
        boolean result = false;
        try {
            result = jdContentService.parseContent(keywords);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(result);
    }

    @GetMapping("/search/{keywords}/{pageNo}/{pageSize}")
    public  List<Map<String, Object>> search(@PathVariable(value = "keywords") String keywords, @PathVariable(value = "pageNo") Integer pageNo, @PathVariable(value = "pageSize") Integer pageSize) {
        List<Map<String, Object>> search = null;
        try {
            //search = jdContentService.search(keywords, pageNo, pageSize);
            search = jdContentService.searchPageHighlightBuilder(keywords, pageNo, pageSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return search;
    }


    @GetMapping("/hightsearch/{keywords}/{pageNo}/{pageSize}")
    public  List<Map<String, Object>> hightsearch(@PathVariable(value = "keywords") String keywords, @PathVariable(value = "pageNo") Integer pageNo, @PathVariable(value = "pageSize") Integer pageSize) {
        List<Map<String, Object>> search = null;
        try {
            search = jdContentService.searchPageHighlightBuilder(keywords, pageNo, pageSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return search;
    }



}

