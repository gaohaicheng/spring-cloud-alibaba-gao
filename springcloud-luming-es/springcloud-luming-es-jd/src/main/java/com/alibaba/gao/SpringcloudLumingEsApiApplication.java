package com.alibaba.gao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudLumingEsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudLumingEsApiApplication.class, args);
    }

}
