package com.alibaba.gao.metrics.service.order;

import java.time.LocalDateTime;

import lombok.Data;

@Data
    public class Order {

        private String orderId;
        private Long userId;
        private Integer amount;
        private LocalDateTime createTime;
        private String channel;
    }

    //控制器和服务类
    