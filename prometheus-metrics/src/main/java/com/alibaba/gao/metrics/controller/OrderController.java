package com.alibaba.gao.metrics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.gao.metrics.service.order.Order;
import com.alibaba.gao.metrics.service.order.OrderService;

@RestController
    public class OrderController {

        @Autowired
        private OrderService orderService;

        @PostMapping(value = "/order")
        public ResponseEntity<Boolean> createOrder(@RequestBody Order order){
            return ResponseEntity.ok(orderService.createOrder(order));
        }
    }

    