package com.alibaba.gao.metrics.service.job;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.okhttp3.OkHttpMetricsEventListener;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import okhttp3.OkHttpClient;

@Component
public class OkHttpMetrics implements MeterBinder {
    

    @Override
    public void bindTo(MeterRegistry meterRegistry) {
        
		/*
		 * new ClassLoaderMetrics().bindTo(meterRegistry); new
		 * JvmMemoryMetrics().bindTo(meterRegistry); new
		 * JvmGcMetrics().bindTo(meterRegistry); new
		 * ProcessorMetrics().bindTo(meterRegistry); new
		 * JvmThreadMetrics().bindTo(meterRegistry);
		 */
        //  通过添加OkHttpMetricsEventListener来收集OkHttpClient指标
        OkHttpClient client = new OkHttpClient.Builder()
            .eventListener(OkHttpMetricsEventListener.builder(meterRegistry, "okhttp.requests")
                .tags(Tags.of("foo", "bar"))
                .build())
            .build();
        
        //  为了配置URI mapper，可以用uriMapper()
//        OkHttpClient client1 = new OkHttpClient.Builder()
//            .eventListener(OkHttpMetricsEventListener.builder(meterRegistry, "okhttp.requests")
//                .uriMapper(req -> req.url().encodedPath())
//                .tags(Tags.of("foo", "bar"))
//                .build())
//            .build();
        

    }

}