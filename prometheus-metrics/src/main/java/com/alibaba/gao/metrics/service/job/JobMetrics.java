package com.alibaba.gao.metrics.service.job;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.okhttp3.OkHttpMetricsEventListener;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import okhttp3.OkHttpClient;

@Component
public class JobMetrics implements MeterBinder {
    public Counter job1Counter;
    public Counter job2Counter;

    public Map<String, Double> map;

    JobMetrics() {
        map = new HashMap<>();
    }

    @Override
    public void bindTo(MeterRegistry meterRegistry) {
         this.job1Counter = Counter.builder("counter_builder_job_counter1")
                .tags(new String[]{"name", "tag_job_counter1"})
                .description("description-Job counter1 execute count").register(meterRegistry);

        this.job2Counter = Counter.builder("counter_builder_job_counter2")
                .tags(new String[]{"name", "tag_job_counter2"})
                .description("description-Job counter2 execute count ").register(meterRegistry);

        Gauge.builder("gauge_builder_job_gauge", map, x -> x.get("x"))
                .tags("name", "tag_job_gauge")
                .description("description-Job gauge")
                .register(meterRegistry);
        
        
        
        
        
		/*
		 * new ClassLoaderMetrics().bindTo(meterRegistry); new
		 * JvmMemoryMetrics().bindTo(meterRegistry); new
		 * JvmGcMetrics().bindTo(meterRegistry); new
		 * ProcessorMetrics().bindTo(meterRegistry); new
		 * JvmThreadMetrics().bindTo(meterRegistry);
		 */
        
        
        

    }

}