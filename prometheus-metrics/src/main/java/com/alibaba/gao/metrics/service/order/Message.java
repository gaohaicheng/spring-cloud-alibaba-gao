package com.alibaba.gao.metrics.service.order;

import lombok.Data;

//实体
@Data
public class Message {

        private String orderId;
        private Long userId;
        private String content;
    }

    