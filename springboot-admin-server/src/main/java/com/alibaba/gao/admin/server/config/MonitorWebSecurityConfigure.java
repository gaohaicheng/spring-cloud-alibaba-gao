//package com.alibaba.gao.admin.server.config;
//
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
//
//import de.codecentric.boot.admin.server.config.AdminServerProperties;
///**
// *      配置  springboot Admin监控权限 
//* @ClassName: MonitorWebSecurityConfigure  
//* @Description: TODO(这里用一句话描述这个类的作用)  
//* @author hcgao  
//* @date 2021年5月27日  
//*
// */
////@Configuration
////@EnableWebSecurity
//public class MonitorWebSecurityConfigure extends WebSecurityConfigurerAdapter {
//    private final String adminContextPath;
//
//    public MonitorWebSecurityConfigure(AdminServerProperties adminServerProperties) {
//        this.adminContextPath = adminServerProperties.getContextPath();
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // @formatter:off
//        SavedRequestAwareAuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
//        successHandler.setTargetUrlParameter("redirectTo");
//        successHandler.setDefaultTargetUrl(adminContextPath + "/");
//
//        http.authorizeRequests()
//                //授予对所有静态资产和登录页面的公共访问权限。
//                .antMatchers(adminContextPath + "/assets/**").permitAll()
//                .antMatchers(adminContextPath + "/login").permitAll()
//                //必须对每个其他请求进行身份验证
//                .anyRequest().authenticated()
//                .and()
//                //配置登录和注销
//                .formLogin().loginPage(adminContextPath + "/login").successHandler(successHandler).and()
//                .logout().logoutUrl(adminContextPath + "/logout").and()
//                //启用HTTP-Basic支持。这是Spring Boot Admin Client注册所必需的
//                .httpBasic().and();
//        // @formatter:on
//    	
//    	
//    	
//    	
//    	
//    	
////    	// Page with login form is served as /login.html and does a POST on /login
////		http.formLogin().loginPage("/login.html").loginProcessingUrl("/login").permitAll();
////		// The UI does a POST on /logout on logout
////		http.logout().logoutUrl("/logout");
////		// The ui currently doesn't support csrf
////		http.csrf().disable();
//// 
////		// Requests for the login page and the static assets are allowed
////		//允许登录页面和静态资源的请求
////		http.authorizeRequests()
////				.antMatchers("/login.html", "/**/*.css", "/img/**", "/third-party/**")
////				.permitAll();
////		// ... and any other request needs to be authorized
////		//这点重要：所有请求都需要认证
////		http.authorizeRequests().antMatchers("/**").authenticated();
//// 
////		// Enable so that the clients can authenticate via HTTP basic for registering
////		http.httpBasic();
//    	
//    	
//    	
//    	
//    }
//}