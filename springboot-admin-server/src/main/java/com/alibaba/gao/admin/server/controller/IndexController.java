package com.alibaba.gao.admin.server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
@RefreshScope
@Api(tags = "SpringbootAdmin-server 首页模块")
@RestController  // http://192.168.1.191:8769/admin-server-monitor/sayHi?name="高"
// http://127.0.0.1:8769/admin-server-monitor/sayHi?name="高"
public class IndexController {  

	/*
	 * @Value("${config.info:}") private String config;
	 * 
	 * @ApiImplicitParam(name = "name",value = "姓名",required = true)
	 * 
	 * @ApiOperation(value = "向客人问好")
	 * 
	 * @GetMapping("/sayHi") public ResponseEntity<String> sayHi(@RequestParam(value
	 * = "name")String name){ return ResponseEntity.ok("Hi:"+name +
	 * "  config : "+config ); }
	 */
	
	
	@Value("${config.info:}")
    private String config;
	
	
    @ApiImplicitParam(name = "name",value = "姓名",required = true)
    @ApiOperation(value = "向客人问好")
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(@RequestParam(value = "name")String name){
        return ResponseEntity.ok("Hi:"+name + "  config : "+config );
    }
	
}