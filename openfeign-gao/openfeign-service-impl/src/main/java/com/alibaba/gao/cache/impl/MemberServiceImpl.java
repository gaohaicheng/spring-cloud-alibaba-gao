package com.alibaba.gao.cache.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.gao.cache.MemberService;

@RestController
public class MemberServiceImpl implements MemberService {
    @Value("${server.port}")
    private String serverPort;

    @Override
    public String getUser(Long userId) {  // http://169.254.251.76:9001/member-openfeign-service/getUser?userId=10
        return "我是会员服务端口号为:" + serverPort;
    }
}
