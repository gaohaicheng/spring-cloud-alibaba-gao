package com.alibaba.gao.order.service.impl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.gao.cache.MemberService;
/**
如果调用服务 添加了 context-path 需要在 @FeignClient 中配置 path = /${servlet.context-path}
server:
  port: 9001
  servlet:
    context-path: /member-openfeign-service
 */
@FeignClient(name = "member-openfeign-service", path = "/member-openfeign-service") // name 为调用服务的服务名称， path 为添加路径
public interface MemberServiceFeign extends MemberService {
    /**
     * 提供会员接口
     *
     * @param userId
     * @return
     */
    @GetMapping("/getUser")
    String getUser(@RequestParam("userId") Long userId);
}
