package com.alibaba.gao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.alibaba.gao.order.service.impl"})
@SpringBootApplication
public class OrderOpenfeignApplication {

    public static void main(String[] args) {
        SpringApplication.run( OrderOpenfeignApplication.class, args );
    }
}