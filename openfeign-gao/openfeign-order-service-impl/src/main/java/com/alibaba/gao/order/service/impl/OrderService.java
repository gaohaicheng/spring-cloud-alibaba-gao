package com.alibaba.gao.order.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderService {
	
    @Autowired
    private MemberServiceFeign memberServiceFeign;

    /**
     * 订单调用会员
     *
     * @return
     */
    @GetMapping("/orderToMember")
    public String orderToMember() {
        String result = memberServiceFeign.getUser(10L);
        return "我是订单服务，调用会员服务接口返回结果:" + result;
    }
}