package com.alibaba.gao.zipkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import zipkin.server.internal.EnableZipkinServer;


@EnableZipkinServer  //开启ZipkinServe
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ZipkinApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinApplication.class, args);
    }
}
