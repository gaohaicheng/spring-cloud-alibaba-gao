package com.alibaba.gao.customscan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义扫描注解  扫描指定包下的client
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AnnotationClientRegistrar.class)
//@ComponentScan(value = "io.mieux.controller")  类似功能
public @interface ReactClientScan {
    String[] packages() default {};
}