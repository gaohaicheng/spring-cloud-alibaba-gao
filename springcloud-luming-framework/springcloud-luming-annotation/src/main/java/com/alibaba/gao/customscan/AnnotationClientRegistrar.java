package com.alibaba.gao.customscan;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 根据指定的包路径进行扫描，如果没有自定包路径，则在当前类所在路径进行扫描
 *
 * @see ReactClientScan 通过注解加载该类
 */
public class AnnotationClientRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(
            AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        Set<String> packages = getPackages(importingClassMetadata);
        ReactClientDefinitionScanner scanner = new ReactClientDefinitionScanner(registry);
        scanner.scan(packages.toArray(new String[]{}));
    }
 
    private Set<String> getPackages(AnnotationMetadata metadata) {
        AnnotationAttributes attributes =
                AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(ReactClientScan.class.getName()));
        String[] packages = attributes.getStringArray("packages");
        Set<String> packagesToScan = new LinkedHashSet<>(Arrays.asList(packages));
        if (packagesToScan.isEmpty()) {
            packagesToScan.add(ClassUtils.getPackageName(metadata.getClassName()));
        }
        return packagesToScan;
    }
 
    /**
     * 继承ClassPathBeanDefinitionScanner，添加filter实现对ReactClient的加载
     */
    private static class ReactClientDefinitionScanner extends ClassPathBeanDefinitionScanner {
 
        public ReactClientDefinitionScanner(BeanDefinitionRegistry registry) {
            super(registry);
            // 利用filter查找目标类，ReactClient.class是用来标记目标类的注解
            addIncludeFilter(new AnnotationTypeFilter(ReactClient.class));
        }
    }
}