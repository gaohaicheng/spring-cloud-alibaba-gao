package com.alibaba.gao.cache.impl;


import com.alibaba.gao.cache.AbstractSaveTheWorldService;

/**
 * 普通人拯救世界
 * @author laihaohua
 */
public class CommonSaveTheWorldServiceImpl extends AbstractSaveTheWorldService {
    private final static int DIE_RATE = 99;

    @Override
    public int getDieRate() {
        return DIE_RATE;
    }
}
