package com.alibaba.gao.controller;


import com.alibaba.gao.cache.SaveTheWorldService;
import com.alibaba.gao.cache.impl.CommonSaveTheWorldServiceImpl;
import com.alibaba.gao.cache.impl.HeroSaveTheWorldImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author gaohaicheng
 *
 * http://localhost:8080/saveTheWorld?name=laihaohua1
 *
 */
@RestController
public class HelloWorldController {
    @Resource
    private CommonSaveTheWorldServiceImpl commonSaveTheWorldService;
    @Resource
    private HeroSaveTheWorldImpl heroSaveTheWorld;

    private static final String HERO = "gao";

    @RequestMapping("/saveTheWorld")
    public String index(String name) {
        SaveTheWorldService saveTheWorldService;
        if(HERO.equals(name)){
            saveTheWorldService = heroSaveTheWorld;
        }else {
            saveTheWorldService = commonSaveTheWorldService;
        }
        return  saveTheWorldService.saveTheWorld(name);
    }
}
