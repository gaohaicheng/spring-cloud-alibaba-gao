package com.luming.test;

import com.alibaba.gao.EnableAutoConfiguration.demo.Hello;
import com.alibaba.gao.EnableAutoConfiguration.demo.HelloProperties;
import com.alibaba.gao.importselector.EnableAccess;
import com.alibaba.gao.cache.Access;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableAccess(type = Access.Type.REST)
/**
 * 1、
 *  Spring Framework提供了两类关于@Enable模块驱动的实现方式:
 *      被@Configuration类和@Bean方法声明的类归类为“注解驱动”。
 *      以ImportSelector或ImportBeanDefinitionRegistrar的实现类归类为“接口编程”。
 *
 * 2、springboot中的类SPI扩展机制
 *      在springboot的自动装配过程中，最终会加载META-INF/spring.factories文件，
 *      而加载的过程是由SpringFactoriesLoader加载的。从CLASSPATH下的每个Jar包中搜寻所有META-INF/spring.factories配置文件，
 *      然后将解析properties文件，找到指定名称的配置后返回。需要注意的是，其实这里不仅仅是会去ClassPath路径下查找，
 *      会扫描所有路径下的Jar包，只不过这个文件只会在Classpath下的jar包中。
 */
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        Access access = context.getBean(Access.class);
        access.start();
        access.stop();


        Hello hello = context.getBean(Hello.class);
        HelloProperties helloProperties = context.getBean(HelloProperties.class);
        System.out.println("Hello   :" + hello.sayHello());
        System.out.println("helloProperties   :" + helloProperties.getMsg());

    }

}
