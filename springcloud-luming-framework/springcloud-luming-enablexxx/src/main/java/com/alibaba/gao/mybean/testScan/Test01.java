package com.alibaba.gao.mybean.testScan;

import org.springframework.stereotype.Component;

/**
 * 类 名: Test01
 * 描 述:  带有默认Spring扫描时会进行管理的注解
 */
@Component
public class Test01 {
}