package com.alibaba.gao.cache.impl;

import com.alibaba.gao.cache.Access;

/**
 * REST实现，只是简单的打印方法
 */
public class RestAccess implements Access {
    @Override
    public void start() {
        System.out.println("rest接入配置");
    }

    @Override
    public void stop() {
        System.out.println("rest接入销毁配置");
    }
}
