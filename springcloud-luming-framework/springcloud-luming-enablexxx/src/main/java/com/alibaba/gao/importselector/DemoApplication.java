package com.alibaba.gao.importselector;

import com.alibaba.gao.cache.Access;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableAccess(type = Access.Type.REST)
/**
 * 在primarySource也就是这里的DemoApplication上使用注解EnableAccess，选择接入方式，就会初始化不通的接入组件
 */
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        Access access = context.getBean(Access.class);
        access.start();
        access.stop();
    }

}
