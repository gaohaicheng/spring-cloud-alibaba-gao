package com.alibaba.gao.mybean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 文档：使用ImportBeanDefinitionRegistrar动态创建自定义Bean到Spring中
 * 链接：http://note.youdao.com/noteshare?id=d52d999e7561b310a86ff6c6534e9b33&sub=F092A235F58D4E5AB4E7FF09F822E015
 */
//@SpringBootApplication
public class MyBeanApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(MyConf.class);
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (int i = 0; i < beanDefinitionNames.length; i++) {
            System.out.println(beanDefinitionNames[i]);
        }
    }

}
