package com.alibaba.gao.myconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * 通过 config配置文件的形式 定义enableXXX 的注解
 */
@Slf4j
@SpringBootApplication
@EnableMyConfig
public class MyBeanConfigDemoApplication {
 
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication
            .run(MyBeanConfigDemoApplication.class, args);
 
        // 验证我们的bean是否成功注入
        log.info(context.getBean("testBean").toString());
    }
}