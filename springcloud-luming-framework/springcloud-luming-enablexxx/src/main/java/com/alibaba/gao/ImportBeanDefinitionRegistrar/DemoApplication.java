package com.alibaba.gao.ImportBeanDefinitionRegistrar;
import com.alibaba.gao.cache.Access;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableAccess(type=Access.Type.RPC)   //type=Access.Type.REST
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        Access access = context.getBean(Access.class);
        access.start();
        access.stop();
    }

}
