package com.alibaba.gao.cache;

/**
 * Access为接入类型的接口，下文的RPC接入和REST接入基于这个实现，定义两个接口，一个为启动，一个停止，内部嵌套一个枚举用于标识是哪一种接入
 */
public interface Access {
    /**
     * 初始化配置
     */
    void start();

    /**
     * 销毁配置
     */
    void stop();

    enum Type{
        REST,
        RPC
    }
}
