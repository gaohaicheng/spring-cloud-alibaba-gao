package com.alibaba.gao.myconfig;

import org.springframework.context.annotation.Bean;

/**
 * @author gaohaicheng
 */
public class MyConfiguration {
 
    @Bean
    public TestBean testBean() {
        return new TestBean();
    }
}