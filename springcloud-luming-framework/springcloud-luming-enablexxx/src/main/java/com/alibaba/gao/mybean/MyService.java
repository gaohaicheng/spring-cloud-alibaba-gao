package com.alibaba.gao.mybean;

import java.lang.annotation.*;

/**
 * 类 名: MyService
 * 描 述: 自定义注解，带有当前注解的类会被注入到Spring容器中
 * Spring怎么知道我们需要扫描哪个地方的带有这个注解的类的，所以我们还需要借助一个ClassPathBeanDefinitionScanner扫描器来获取我们所需要创建的Bean；
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
public @interface MyService {

}