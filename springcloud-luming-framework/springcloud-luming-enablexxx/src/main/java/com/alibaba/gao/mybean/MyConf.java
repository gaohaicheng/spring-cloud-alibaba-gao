package com.alibaba.gao.mybean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 从输入结果我们可以看到，使用@Import注解引入的类，是不会被加载到Spring容器中的。同时，我们指定自定义的类扫描器在扫描的路径下，
 * 即使出现了默认Spring本该扫描的注解@Component时它也没有将其加载到Spring容器中。而带有我们特定标识@MyService的类则被加载到Spring容器中了，
 * 而另外的一个什么标识都没有的类，也没有被加载到容器中；
 */
@Configuration
@Import(MyAutoConfiguredMyBatisRegistrar.class)
//@ComponentScan("com.alibaba.gao.mybean")   // 扫描 spring自己的包  @service  @component 等
public class MyConf {

}