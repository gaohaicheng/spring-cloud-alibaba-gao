package com.alibaba.gao.importselector;

import com.alibaba.gao.cache.Access;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;
/**
 * 自定义注解EnableAccess
 * 接入类型为RPC或者REST，AccessImportSelector在下一步骤实现
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
// @Import 注解中可以放 1、 @Configuration注解类， 2、ImportSelector   ；3、ImportBeanDefinitionRegistrar
//@Import({AccessImportSelector.class, HelloAutoConfiguration.class})
@Import(AccessImportSelector.class)
public @interface EnableAccess {
    /**
     * 接入类型
     * @return
     */
    Access.Type type();
}
