package com.alibaba.gao.ImportBeanDefinitionRegistrar;
import com.alibaba.gao.cache.Access;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
// @Import 注解中可以放 1、 @Configuration注解类， 2、ImportSelector   ；3、ImportBeanDefinitionRegistrar
//@Import({AccessImportSelector.class, HelloAutoConfiguration.class})
@Import(AccessImportBeanDefinitionRegistrar.class)
public @interface EnableAccess {
    /**
     * 接入类型
     * @return
     */
    Access.Type type();
}
