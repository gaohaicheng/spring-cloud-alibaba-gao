package com.alibaba.gao.myconfig;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author gaohaicheng
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
// @Import 注解中可以放 1、 @Configuration注解类， 2、ImportSelector   ；3、ImportBeanDefinitionRegistrar
//@Import({AccessImportSelector.class, HelloAutoConfiguration.class})
@Import(MyConfiguration.class)
public @interface EnableMyConfig {
}