package com.alibaba.gao.cache.impl;

import com.alibaba.gao.cache.Access;

/**
 * RPC实现
 */
public class RpcAccess implements Access {
    @Override
    public void start() {
        System.out.println("rpc接入配置");
    }

    @Override
    public void stop() {
        System.out.println("rpc接入销毁配置");
    }
}
