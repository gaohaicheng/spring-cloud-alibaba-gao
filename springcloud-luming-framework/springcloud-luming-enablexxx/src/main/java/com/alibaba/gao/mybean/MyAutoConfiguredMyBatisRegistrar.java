package com.alibaba.gao.mybean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 类 名: MyAutoConfiguredMyBatisRegistrar
 * 描 述: 测试动态注入Bean到Spring容器中
 */
public class MyAutoConfiguredMyBatisRegistrar implements ImportBeanDefinitionRegistrar, ResourceLoaderAware , BeanFactoryAware  {

    private ResourceLoader resourceLoader;
    /**
     * 动态置顶扫描包路径下特殊的类加载到Bean中
     * @param importingClassMetadata
     * @param registry
     */
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 当前MyClassPathBeanDefinitionScanner已被修改为扫描带有指定注解的类
        MyClassPathBeanDefinitionScanner scanner = new MyClassPathBeanDefinitionScanner(registry, false);
        scanner.setResourceLoader(resourceLoader);
        scanner.registerFilters();
        scanner.doScan("com.alibaba.gao.mybean.testScan");
    }


    /**
     *  将一个 bean 直接注入到 BeanDefinitionRegistry 中
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 获取容器中已经存在的Bean的名字
        boolean definition1 = registry.containsBeanDefinition("com.nmys.story.springCore.springioc.importBean.Cat");
        boolean definition2 = registry.containsBeanDefinition("com.nmys.story.springCore.springioc.importBean.Dog");
        if (definition1 && definition2) {
            // 将需要放入容器中的Bean用RootBeanDefinition来包装一下。
            RootBeanDefinition beanDefinition = new RootBeanDefinition(Pig.class);  // 注入名字为  pig的 beanDefinition
            // 向容器中注册这个Bean，并给定一个名字。
            registry.registerBeanDefinition("pig", beanDefinition);
        }
    }
    */


    /**
     * 获取Spring中的一些元数据
     * @param resourceLoader
     */
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    }
}