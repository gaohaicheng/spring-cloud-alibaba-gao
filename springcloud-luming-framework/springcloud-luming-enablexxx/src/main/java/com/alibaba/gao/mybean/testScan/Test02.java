package com.alibaba.gao.mybean.testScan;

import com.alibaba.gao.mybean.MyService;

/**
 * 类 名: Test01
 * 描 述: 带有我们自定义的注解，不会被默认的Spring扫描器所扫描管理
 */
@MyService
public class Test02 {
}