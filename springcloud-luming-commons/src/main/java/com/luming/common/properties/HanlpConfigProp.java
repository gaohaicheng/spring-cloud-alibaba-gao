package com.luming.common.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "hanlp", ignoreUnknownFields = false)
//@PropertySource(value = "classpath:hanlp.properties")
public class HanlpConfigProp {

    //@Value("${hanlp.root}")
    private String root;

    //@Value("${hanlp.dataZipFileURL}")
    private String dataZipFileURL;

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getDataZipFileURL() {
        return dataZipFileURL;
    }

    public void setDataZipFileURL(String dataZipFileURL) {
        this.dataZipFileURL = dataZipFileURL;
    }
}