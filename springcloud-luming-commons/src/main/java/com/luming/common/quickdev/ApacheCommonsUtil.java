package com.luming.common.quickdev;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;

import java.text.ParseException;
import java.util.*;

/**
 *  apache commons工具类库
 *  apache commons是最强大的，也是使用最广泛的工具类库，里面的子库非常多，下面介绍几个最常用的
 *  建议使用commons-lang3，优化了一些api，原来的commons-lang已停止更新
 *   <dependency>
 *     <groupId>org.apache.commons</groupId>
 *     <artifactId>commons-lang3</artifactId>
 *     <version>3.12.0</version>
 * </dependency>
 *
 */
public class ApacheCommonsUtil {

    /**
     * List集合拼接成以逗号分隔的字符串
     */
    public static void stringEmpty(String str) {
        StringUtils.isEmpty(str);
        StringUtils.isNotEmpty(str);

        // 判空的时候，会去除字符串中的空白字符，比如空格、换行、制表符
        StringUtils.isBlank(str);
        StringUtils.isNoneBlank(str);

    }

    /**
     *  首字母转成大写
     */
    public static void strCapitalize(){
        String str = "yideng";
        String capitalize = StringUtils.capitalize(str);
        System.out.println(capitalize); // 输出Yideng
    }


    /**
     *  当一个方法需要返回两个及以上字段时，我们一般会封装成一个临时对象返回，现在有了Pair和Triple就不需要了
     */
    public static void bodyPairTriple(){
        // 返回两个字段
        ImmutablePair<Integer, String> pair = ImmutablePair.of(1, "yideng");
        System.out.println(pair.getLeft() + "," + pair.getRight()); // 输出 1,yideng
        // 返回三个字段
        ImmutableTriple<Integer, String, Date> triple = ImmutableTriple.of(1, "yideng", new Date());
        System.out.println(triple.getLeft() + "," + triple.getMiddle() + "," + triple.getRight()); // 输出 1,yideng,Wed Apr 07 23:30:00 CST 2021

    }


    /**
     *  重复拼接字符串
     */
    public static void stringRepeat(){
        String str = StringUtils.repeat("ab", 2);
        System.out.println(str); // 输出abab
    }

    /**
     *  格式化日期
     *  再也不用手写SimpleDateFormat格式化了
     */
    public static void dateFormat() {
        try {
            // Date类型转String类型
            String date = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
            System.out.println(date); // 输出 2021-05-01 01:01:01
            // String类型转Date类型
            Date date1 = DateUtils.parseDate("2021-05-01 01:01:01", "yyyy-MM-dd HH:mm:ss");
            // 计算一个小时后的日期
            Date date2 = DateUtils.addHours(new Date(), 1); // 小时  支持 负数
            Date date3 = DateUtils.addDays(new Date(), 1); // 天
            DateUtils.addWeeks(new Date(), 1); // 周
        }catch (Exception e){

        }
    }




    /************************************** commons-collections 集合工具类 ******************************************************/



    /**
     *  封装了集合判空的方法，以下是源码：
     */
    public static void stringCapitalize(){
        List listA = new ArrayList();
        List listB = new ArrayList();

        boolean empty = CollectionUtils.isEmpty(listA);

        // 两个集合取交集
        Collection<String> collection1 = CollectionUtils.retainAll(listA, listB);
        // 两个集合取并集
        Collection<String> collection2 = CollectionUtils.union(listA, listB);
        // 两个集合取差集
        Collection<String> collection3 = CollectionUtils.subtract(listA, listB);
    }




    /************************************** common-beanutils 操作对象 ******************************************************/


    public static void setBean(){
        try {
            User user = new User();
            BeanUtils.setProperty(user, "id", 1);
            BeanUtils.setProperty(user, "name", "yideng");
            System.out.println(BeanUtils.getProperty(user, "name")); // 输出 yideng
            System.out.println(user); // 输出 {"id":1,"name":"yideng"}


            //对象和map互转
            // 对象转map
            Map<String, String> map = BeanUtils.describe(user);
            System.out.println(map); // 输出 {"id":"1","name":"yideng"}
            // map转对象
            User newUser = new User();
            BeanUtils.populate(newUser, map);
            System.out.println(newUser); // 输出 {"id":1,"name":"yideng"}

        }catch (Exception e){}
    }




    public static void main(String[] args) {
        ApacheCommonsUtil.dateFormat();
        ApacheCommonsUtil.bodyPairTriple();
        ApacheCommonsUtil.stringRepeat();

    }


}
