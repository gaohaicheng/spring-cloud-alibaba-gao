package com.luming.common.quickdev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  Java自带工具方法
 */
public class JavaQuickDevUtil {

    /**
     * List集合拼接成以逗号分隔的字符串
     */
    public static void ListDemo() {
        // 如何把list集合拼接成以逗号分隔的字符串 a,b,c
        List<String> list = Arrays.asList("a", "b", "c");
        // 第一种方法，可以用stream流
        String join = list.stream().collect(Collectors.joining(","));
        System.out.println(join); // 输出 a,b,c
        // 第二种方法，其实String也有join方法可以实现这个功能
        String join2 = String.join(",", list);
        System.out.println(join2); // 输出 a,b,c
    }

    /**
     *  比较两个字符串是否相等，忽略大小写
     */
    public static void stringCompare(){
        String strA = "11a";
        String strB = "11A";
        if (strA.equalsIgnoreCase(strB)) {
            System.out.println("相等");
        }
    }


    /**
     *  两个List集合取交集
     */
    public static void getListIntersection(){
        List<String> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        List<String> list2 = new ArrayList<>();
        list2.add("a");
        list2.add("b");
        list2.add("d");
        list1.retainAll(list2);
        System.out.println(list1); // 输出[a, b]
    }



    public static void main(String[] args) {
        JavaQuickDevUtil.ListDemo();
        JavaQuickDevUtil.stringCompare();
        JavaQuickDevUtil.getListIntersection();
    }


}
 
