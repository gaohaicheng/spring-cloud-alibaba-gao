package com.luming.common;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *  starter
 *
 * @author anyone
 * @since Tue Jun 08 10:55:51 CST 2021
 */
@SpringBootApplication(scanBasePackages = { "com.luming.common" })
//@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
@EnableDiscoveryClient
@EnableTransactionManagement
//@MapperScan("com.enn.easyprd.dal.mapper")
//@EnableDubbo(scanBasePackages = {"com.enn.easyprd.facade"})
@EnableAsync
//@EnableConfigurationProperties
public class ApplicationRunner implements CommandLineRunner{
    private static final Logger logger=LoggerFactory.getLogger(ApplicationRunner.class);

    public static void main(String[] args) {
        SpringApplication.run(ApplicationRunner.class, args);
    }

    /*public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(JdbcYmlConfiguration.class);
        System.out.println(ctx.getBean(JdbcYmlProperty.class).toString());
    }*/



    @Override
    public void run(String... args) throws Exception {
        logger.info("app start");
    }
}
