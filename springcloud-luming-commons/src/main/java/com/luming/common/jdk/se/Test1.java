package com.luming.common.jdk.se;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName Test1.java
 * @Description TODO
 * @createTime 2022年07月14日 09:16:00
 */
public class Test1 {

    public static void main(String[] args) {
        /*String str1 = "abc";
        String str2 = "a" + new String("bc");

        System.out.println(str1 == str2);

        String str3 = new String("java");
        String str4 = new String("java");
        System.out.println(str3 == str4);*/


        String str1 = new String("aa");
        str1.intern();
        String str2 = "aa";
        System.out.println(str1 == str2);

        String str3 = new String("a") + new String("a");
        str3.intern();
        String str4 = "aa";
        System.out.println(str3 == str4);



    }

}
