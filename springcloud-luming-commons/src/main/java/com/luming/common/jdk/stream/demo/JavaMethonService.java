package com.luming.common.jdk.stream.demo;

import io.lettuce.core.output.KeyValueStreamingChannel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName JavaMethonService.java
 * @Description TODO
 * @createTime 2021年11月25日 15:19:00
 */
public class JavaMethonService {

    private static final Map<String,Class> CIM_DATASYNC_EVENT_MAPPING = new HashMap<>();

    static {
        CIM_DATASYNC_EVENT_MAPPING.put("CIM_STATION_DELETED", Person.class);
        CIM_DATASYNC_EVENT_MAPPING.put("CIM_ATTRS_UPDATED", Dog.class);
    }


    public void aa(String type, String param){
        System.out.println("aa method   type :" +type+"   parma:"+param);
    }

    public void bb(String type, String param){
        System.out.println("bb method   type :" +type+"   parma:"+param);
    }

    public void cc(String type, String param){
        System.out.println("bb method   type :" +type+"   parma:"+param);
    }


    public static void main(String[] args) {



        Person person = new Person("李四", 18, 5000);
        String param = JsonUtils.object2Json(person);
        String type = "CIM_STATION_DELETED";


//
//        JavaMethonService service = new JavaMethonService();
//        Map<String, KeyValueStreamingChannel<String, String>> CimServiceMethod = new HashMap<>();
//        CimServiceMethod.put("CIM_STATION_DELETED", service::aa);
//        CimServiceMethod.put("CIM_ATTRS_UPDATED", service::bb);
//        CimServiceMethod.put("CIM_DEVICE_CREATED", service::cc);
//
//
//        CimServiceMethod.get(type).onKeyValue(type,param);


        Class<?> paramClass = CIM_DATASYNC_EVENT_MAPPING.get(type);
        System.out.println(paramClass);
        Object paramObj = JsonUtils.json2Object(param, paramClass);
        System.out.println(param.getClass());
        Person parameter = (Person)paramObj;
        System.out.println(parameter);


    }


}
