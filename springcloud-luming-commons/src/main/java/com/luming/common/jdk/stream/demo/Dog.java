package com.luming.common.jdk.stream.demo;

import lombok.Data;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName Dog.java
 * @Description TODO
 * @createTime 2021年11月25日 15:27:00
 */
@Data
public class Dog {

    private String name;

    private String color;

}
