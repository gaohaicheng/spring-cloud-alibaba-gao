package com.luming.common.jdk.stream.demo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName Person.java
 * @Description TODO
 * @createTime 2021年11月12日 09:29:00
 */
@Data
@NoArgsConstructor
public class Person {

    private String name;

    private int age;

    private int salary;

    private String sex;

    private String area;

    public Person(String name, int age, int salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public Person(String name, int age, int salary, String sex, String area) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
        this.area = area;
    }
}
