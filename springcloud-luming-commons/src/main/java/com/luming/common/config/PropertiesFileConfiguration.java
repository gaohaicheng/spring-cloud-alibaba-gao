package com.luming.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @PropertySource默认不支持yaml读取，我们改成@Value注解也是可以读取的，不过属性一堆的话，一个一个读取也是很繁琐的，
 * 通过网上找资料和自己实验验证，发现是可以实现对yaml支持
 * 然后，为什么@PropertySource注解默认不支持？
 */

@Configuration
@ComponentScan({"com.luming.common.properties" })
//@ConfigurationProperties(prefix = "yml.jdbc")
@PropertySource(value = {"classpath:hanlp.properties"} ,ignoreResourceNotFound=true)  //propertysource/jdbc.yml
public class PropertiesFileConfiguration {

}