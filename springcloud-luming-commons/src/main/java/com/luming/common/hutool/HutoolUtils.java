package com.luming.common.hutool;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.ReflectUtil;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName HutoolUtils.java
 * @Description TODO
 * @createTime 2022年05月26日 14:04:00
 */
@Slf4j
public class HutoolUtils {

    @Operation(description = "ReflectUtil使用：JAVA反射工具类")
    @GetMapping("/reflectUtil")
    public static void reflectUtil(){
        // 获取某个类的所有方法
        Method[] methods = ReflectUtil.getMethods(PmsBrand.class);

        // 获取某个类的指定方法
        Method method = ReflectUtil.getMethod(PmsBrand.class, "getId");

        // 使用反射来创建对象
        PmsBrand pmsBrand = ReflectUtil.newInstance(PmsBrand.class);

        // 反射执行对象的方法
        ReflectUtil.invoke(pmsBrand, "setId", 1);
    }



    public static void getAnnotationInfo(){
        // 获取指定类、方法、字段、构造器上的注解列表
        Annotation[] annotationList = AnnotationUtil.getAnnotations(HutoolController.class, false);
        log.info("annotationUtil annotations:{}", annotationList);

        // 获取指定类型注解
        ApiObj api = AnnotationUtil.getAnnotation(HutoolController.class, ApiObj.class);
        log.info("annotationUtil api value:{}", api.description());

        // 获取指定类型注解的值
        Object annotationValue = AnnotationUtil.getAnnotationValue(HutoolController.class, RequestMapping.class);


        for (Method method : HutoolController.class.getMethods()) {
            System.out.println(Arrays.toString(method.getAnnotations()));
        }
    }


    public static void main(String[] args) {
        HutoolUtils.getAnnotationInfo();
    }




}
