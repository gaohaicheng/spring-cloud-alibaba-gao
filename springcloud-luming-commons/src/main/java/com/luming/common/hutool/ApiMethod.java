package com.luming.common.hutool;

import java.lang.annotation.*;
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ApiMethod {
    String method() default "";

    String[] tags() default {};

    String summary() default "";

    String description() default "";
}
