package com.luming.common.hutool;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName HutoolController.java
 * @Description TODO
 * @createTime 2022年05月26日 14:16:00
 */

@RestController
@ApiObj(description = "hutoolController 测试注解 @ApiObj")
public class HutoolController {

    private String name;

    @ApiMethod(description="hutoolController 测试注解 @ApiMethod  方法 setName")
    public void setName(String name){
        this.name = name;
    }


    @RequestMapping
    public String getById(String id){
        return  "sssss";
    }
}
