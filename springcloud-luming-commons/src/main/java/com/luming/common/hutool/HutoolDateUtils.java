package com.luming.common.hutool;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName HutoolDateUtils.java
 * @Description TODO
 * @createTime 2022年09月02日 17:10:00
 *
 */
public class HutoolDateUtils {

    public static void main(String[] args) {
        Date date = new Date();
        Date  beforeYesterday = DateUtil.offsetDay(new Date(), -2);
        Date dateTime = DateUtil.offsetDay(date, -1);

        Date start = DateUtil.parse("2022-08-30 04:30:00", DatePattern.NORM_DATETIME_PATTERN).toJdkDate();
        Date end = DateUtil.parse("2022-09-02 21:00:00", DatePattern.NORM_DATETIME_PATTERN).toJdkDate();

        if(DateUtil.compare(end, DateUtil.endOfDay(dateTime)) > 0){
            end = DateUtil.beginOfDay(dateTime).toJdkDate();
        }
        if(DateUtil.compare(start, DateUtil.endOfDay(dateTime)) > 0){
            start = DateUtil.beginOfDay(dateTime).toJdkDate();
        }
        Date statDat = DateUtil.beginOfDay(start).toJdkDate();
        Date endDat = DateUtil.beginOfDay(end).toJdkDate();


        long intervalDay = DateUtil.betweenDay(statDat,endDat , false);
        System.out.println(intervalDay);
        System.out.println(statDat);
        System.out.println(endDat);

        for (int i = 0; i <= intervalDay; i++) {
            Date  runDateTime = DateUtil.offsetDay(statDat, i);
            System.out.println(runDateTime);
            System.out.println( DateUtil.beginOfDay(runDateTime));
            System.out.println( DateUtil.endOfDay(runDateTime));
            System.out.println( "---------------------");
        }


    }


}
