package com.luming.common.hutool;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author gaohaicheng
 */
@Target(ElementType.METHOD) //只对方法上加该注解有效
@Retention(RetentionPolicy.RUNTIME) //运行时有效
public @interface ToolWorkMethod {

	//声明一个无默认值的属性
	public String taskRefCode() default "";

}
