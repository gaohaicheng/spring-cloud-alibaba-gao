package com.luming.common.hutool;

import java.lang.annotation.*;

/**
 * @author gaohaicheng
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface WorkerService {

    String name() default "";

    String value() default "";

    String description() default "";

}
