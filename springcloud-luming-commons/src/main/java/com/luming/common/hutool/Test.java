package com.luming.common.hutool;

import cn.hutool.core.util.ReflectUtil;
import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author hcgao
 * @version 1.0.0
 * @ClassName Test.java
 * @Description TODO
 * @createTime 2022年07月04日 18:08:00
 */
public class Test {

    public void annotationTest(){
    // 获取某个类的所有方法
        Method[] methods = ReflectUtil.getMethods(PmsBrand.class);
        for (Method method : methods) {
            ToolWorkMethod annotation = method.getAnnotation(ToolWorkMethod.class);
            if(annotation != null) {
                System.out.println(annotation.taskRefCode());
            }
        }





        //实例化worker
        //扫描获取注解
        Reflections f = new Reflections("com.ennew.ai.ruleflow.biz.service.orche.client");
        Set<Class<?>> workerSet=f.getTypesAnnotatedWith(WorkerService.class);
        for (Class<?> c : workerSet) {
            PmsBrand worker = (PmsBrand)ReflectUtil.newInstance(c);
        }




    }

}
