package com.luming.common.hutool;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ApiObj {
    String method() default "";

    String[] tags() default {};

    String summary() default "";

    String description() default "";
}
