package com.luming.common.yaml.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@Data
@ConfigurationProperties(prefix = "yml.jdbc")
public class JdbcYmlProperty {

    //@Value("${yml.jdbc.url}")
    private String url;

    private String driverClassName;

    private String username;

    private String password;
    
    // 省略getter setter toString
}