package com.luming.common.service;

import cn.hutool.core.io.FileUtil;
import com.luming.common.file.DownloadUtil;
import com.luming.common.properties.HanlpConfigProp;
import com.luming.common.yaml.bean.JdbcYmlProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component
public class AiHanlpBootService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    HanlpConfigProp config;

    @Autowired
    JdbcYmlProperty yamlconfig;

    @PostConstruct
    public void executDownloadData(){
        System.out.println("=======================config文件内容  ： "+ config.getDataZipFileURL() + "     " + config.getRoot()  );
        System.out.println("=======================yamlconfig   ： "+ yamlconfig.getUrl() + "     " + yamlconfig.getUsername() );

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    logger.debug("=======================config文件内容  ： "+ config.getDataZipFileURL() + "     " + config.getRoot());
                    /*String hanlpRoot = config.getRoot();
                    String fileUrl = config.getDataZipFileURL();
                    DownloadUtil.downloadFile(fileUrl,hanlpRoot ,"data.zip", null);
                    String filePath = hanlpRoot + File.separator + "data.zip";
                    File file = new File(filePath);
                    DownloadUtil.unZipFiles(file,hanlpRoot + File.separator);
                    FileUtil.del(file);
                    logger.debug("文件删除成功  ： " + filePath);*/
                } catch (Exception e) {
                    logger.debug("文件copy失败");
                    return;
                }
            }
        }).start();
    }

}
